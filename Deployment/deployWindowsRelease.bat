@echo off

::From Qt Creator arguments #1 should be %{buildDir} and %2 %{sourceDir} %3 "<packageName>"
::Use  {buildDir} %{sourceDir} "Windows"
set SOURCEBINARYPATH=%1
set PROJECTROOT=%2
set PACKAGENAME=%3

set ZIPNAME=TimeToPicNoder-%PACKAGENAME%

set PROJECT_FOLDER=%PROJECTROOT%
set DEPLOYMENTROOT=%PROJECT_FOLDER%\deployment
set DEPLOYMENTWINDOWS=%DEPLOYMENTROOT%\Windows




set SHADOWBUILDDIR=%SOURCEBINARYPATH%


:: Setup tools
set ZIPTOOL="C:\Program Files\7-Zip\7z.exe"
set NSISTOOL="C:\Program Files (x86)\NSIS\makensis"

::Set paths 

::Clean
rmdir /S /Q %DEPLOYMENTWINDOWS%\
mkdir %DEPLOYMENTWINDOWS%

::Copy exe 
xcopy %SHADOWBUILDDIR%\release\TimeToPicNoder.exe %DEPLOYMENTWINDOWS% /e /i /y

::Copy dlls
::echo copy dll's
::xcopy %DLLPATH%\*.* %DEPLOYMENTWINDOWS% /e /i /y

::windeploy qt 
windeployqt --qmldir %PROJECT_FOLDER%\src\QML %DEPLOYMENTWINDOWS%\TimeToPicNoder.exe



::Copy docs
echo copy docs
xcopy %PROJECT_FOLDER%\doc-TimeToPicNoder\*.txt %DEPLOYMENTWINDOWS%\doc-TimeToPicNoder /e /i /y
xcopy %PROJECT_FOLDER%\doc-TimeToPicNoder\*.pdf %DEPLOYMENTWINDOWS%\doc-TimeToPicNoder /e /i /y

::Copy exampleImages
xcopy %PROJECT_FOLDER%\testImages\*.png %DEPLOYMENTWINDOWS%\testImages /e /i /y

::Copy scripts
echo copy scripts
xcopy %PROJECT_FOLDER%\src\scripts\*.* %DEPLOYMENTWINDOWS%\ /e /i /y


::Copy examplelogs
echo copy examplelogs
xcopy %PROJECT_FOLDER%\examplelog\*.txt %DEPLOYMENTWINDOWS% /e /i /y

:: delete licence keys and etc
del %DEPLOYMENTWINDOWS%\*.ini
del %DEPLOYMENTWINDOWS%\log*.txt
del %DEPLOYMENTWINDOWS%\*.dat

::NSIS
del %DEPLOYMENTROOT%\TimeToPicNoderInstaller.exe
%NSISTOOL% %DEPLOYMENTROOT%\TimeToPicNoder.nsi

::make package
del %DEPLOYMENTROOT%\%ZIPNAME%.zip
%ZIPTOOL% a -r -tzip %DEPLOYMENTROOT%\%ZIPNAME% %DEPLOYMENTWINDOWS%

::md5
echo "Do MD5"
Certutil -hashfile %DEPLOYMENTROOT%\%ZIPNAME%.zip md5 > %DEPLOYMENTROOT%\%ZIPNAME%_md5.txt

echo "Done"

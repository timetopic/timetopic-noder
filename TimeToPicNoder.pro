/*

# TimeToPic Noder visualizes networks real time. Tool for fast prototyping.

# Copyright (c) 2015 Herwood Technologies Oy,
# Erkki Salonen erkki@herwoodtechnologies.com

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#VISUAL STUDIO COMPILER NEEDED

TEMPLATE = app

QT += qml quick widgets websockets

SOURCES += src/App/main.cpp \    
    src/Graphics/nodeview.cpp \
    src/Graphics/nodescene.cpp \
    src/App/app.cpp \
    src/Graphics/nodegraphicsview.cpp \
    src/APIandComms/networkaccessandapi.cpp \
    src/App/graphs.cpp \
    src/App/nodeitem.cpp \
    src/Graphics/graphicsnodeitem.cpp \
    src/App/graphnode.cpp \
    src/TestData/testdatagen.cpp \
    src/timetopic-logger-examples/src/qt/timetopiclogger.cpp \
    src/App/graphlink.cpp \
    src/App/worker.cpp \
    src/Graphics/colorgenerator.cpp

RESOURCES += src/QML/qml.qrc \
    gfx/gfxassets.qrc \
    testImages/testimages.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

RC_ICONS = gfx/TimeToPicNoder.ico

# Default rules for deployment.
include(deployment.pri)

INCLUDEPATH+=src \
             src/APIandComms \
             src/App \
             src/Graphics \
             src/QML \
             src/TestData \
             src/timetopic-logger-examples/src/qt

HEADERS += \
    src/APIandComms/networkaccessandapi.h \
    src/App/app.h \
    src/Graphics/nodegraphicsview.h \
    src/Graphics/nodescene.h \
    src/Graphics/nodeview.h \
    src/App/graphs.h \
    src/App/nodeitem.h \
    src/Graphics/graphicsnodeitem.h \
    src/App/graphnode.h \
    src/TestData/testdatagen.h \
    src/timetopic-logger-examples/src/qt/timetopiclogger.h \
    src/App/graphlink.h \
    src/App/worker.h \
    src/Graphics/colorgenerator.h

# You need create lib your self. See instructions from http://www.ogdf.net/doku.php/tech:installvcc
win32:CONFIG(release, debug|release):    LIBS += -L$$PWD/lib/ogdf-snapshot-2018-03-28/OGDF-snapshot/release/ -logdf
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/lib/ogdf-snapshot-2018-03-28/OGDF-snapshot/debug/ -logdf

INCLUDEPATH += $$PWD/lib/ogdf-snapshot-2018-03-28/OGDF-snapshot/include/
DEPENDPATH += $$PWD/lib/ogdf-snapshot-2018-03-28/OGDF-snapshot/include/

win32:CONFIG(release, debug|release):    LIBS += -L$$PWD/lib/ogdf-snapshot-2018-03-28/OGDF-snapshot/release/ -lcoin
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/lib/ogdf-snapshot-2018-03-28/OGDF-snapshot/debug/ -lcoin

INCLUDEPATH += $$PWD/lib/ogdf-snapshot-2018-03-28/OGDF-snapshot/include/
DEPENDPATH += $$PWD/lib/ogdf-snapshot-2018-03-28/OGDF-snapshot/include/

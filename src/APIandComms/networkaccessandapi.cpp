/*

TimeToPic Noder visualizes networks real time. Tool for fast prototyping.

Copyright (c) 2015 Herwood Technologies Oy,
Erkki Salonen erkki@herwoodtechnologies.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


#include <QObject>
#include "networkaccessandapi.h"

const QString connAddress = "ws://127.0.0.1:4056";
NetworkAccessAndAPI::NetworkAccessAndAPI(): mWSocket(NULL)
{

}

void NetworkAccessAndAPI::InitThis() {

    TryConnecting();    
}




void NetworkAccessAndAPI::TryConnecting()
{
    if ( mWSocket == NULL) {
        mWSocket = new QWebSocket();

        connect(mWSocket,SIGNAL(connected()),this,SLOT(WSconnected()));
        connect(mWSocket,SIGNAL(disconnected()),this,SLOT(WSdisconnected()));
        connect(mWSocket,SIGNAL(error(QAbstractSocket::SocketError))
                ,this,SLOT(WSerror(QAbstractSocket::SocketError)));
        connect(mWSocket,SIGNAL(textMessageReceived(QString)),SLOT(WStextMessageReceived(QString)));
    }

    emit connectionStatusSignal(QString("Connecting to %1").arg(connAddress));
    mWSocket->open(QUrl(connAddress));

}

void NetworkAccessAndAPI::testDataMessageReceivedSlot(QString message)
{
    WStextMessageReceived(message);
}

void NetworkAccessAndAPI::WSconnected()
{
    emit connectionStatusSignal(QString("Connected to %1").arg(connAddress));
}

void NetworkAccessAndAPI::WSdisconnected()
{
    emit connectionStatusSignal("No connection");
}

void NetworkAccessAndAPI::WSerror(QAbstractSocket::SocketError error)
{
    if ( error == QAbstractSocket::ConnectionRefusedError) {
        emit connectionStatusSignal(QString("Connection to '%1' failed").arg(connAddress));
    }

}

void NetworkAccessAndAPI::WStextMessageReceived(QString message)
{
    QStringList messages = message.split("\n");

    foreach (QString dataMsg, messages) {
        QStringList msgFragments = dataMsg.split(";");
        if (msgFragments.size()>2) {            
            const QString nodesLoglines = "nodes";
            if (msgFragments.at(1) == nodesLoglines) {
                msgFragments.removeFirst(); // drop timestamp
                msgFragments.removeFirst(); // drop nodesLoglines

                emit newNetworkCommandSignal(msgFragments);
            }
        }
    }

}


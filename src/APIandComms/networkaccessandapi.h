#ifndef NETWORKACCESSANDAPI_H
#define NETWORKACCESSANDAPI_H

#include <QObject>
#include <QtWebSockets/QWebSocket>
#include "worker.h"

class NetworkAccessAndAPI : public worker
{
    Q_OBJECT
public:
    NetworkAccessAndAPI();

    virtual void InitThis();

    void TryConnecting();

signals:
    void newNetworkCommandSignal(QStringList messageParts);
    void connectionStatusSignal(QString connectionStatus);
public slots:
    void    testDataMessageReceivedSlot(QString message);
private slots:
    void	WSconnected();
    void	WSdisconnected();
    void	WSerror(QAbstractSocket::SocketError error);
    void	WStextMessageReceived(QString message);

private:
    QWebSocket *mWSocket;
};

#endif // NETWORKACCESSANDAPI_H

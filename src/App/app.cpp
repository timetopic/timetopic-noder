/*

TimeToPic Noder visualizes networks real time. Tool for fast prototyping.

Copyright (c) 2015 Herwood Technologies Oy,
Erkki Salonen erkki@herwoodtechnologies.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "app.h"
#include "graphicsnodeitem.h"
#include "graphs.h"

#include <QDateTime>
#include <QDebug>

const int sceneEdgeLen = 50000;
const QRect defaultScene(0, 0, sceneEdgeLen, sceneEdgeLen);

static App *gApp = NULL;

App *App::Instance() {
  if (gApp == NULL) {
    gApp = new App;
  }
  return gApp;
}

App::App(QObject *parent)
    : QObject(parent), mScene(NULL), mAccess(NULL), mGrapher(NULL),
      mMessageParserProblemCounter(0), mProcessedMsgsCounter(0),
      mFastAutoLayoutReg(false) {
  mAutoLayoutEnabled = true;
  m_disableCoordinates = false;

  mAccess = new NetworkAccessAndAPI;
  mScene = new NodeScene;
  mView = new NodeGraphicsView;
  mView->setScene(mScene);
  mView->setSceneRect(defaultScene);
  mGraphNodes = new QHash<QString, GraphNode *>;
  mGraphLinks = new QHash<QString, GraphLink *>;

  mGrapher = new Graphs;
  mTestData = new TestDataGen;

  connect(mAccess, SIGNAL(newNetworkCommandSignal(QStringList)), this,
          SLOT(handleNetworkCommandSlot(QStringList)), Qt::QueuedConnection);

  connect(mAccess, SIGNAL(connectionStatusSignal(QString)), this,
          SLOT(connectionStatusChanged(QString)), Qt::QueuedConnection);

  connect(mTestData, SIGNAL(testDataMessageAvailableSignal(QString)), mAccess,
          SLOT(testDataMessageReceivedSlot(QString)), Qt::QueuedConnection);

  connect(mGrapher, SIGNAL(nodeCoordCalculated(QHash<QString, NodeItem> *)),
          this, SLOT(updateCoordsSlot(QHash<QString, NodeItem> *)),
          Qt::QueuedConnection);

  connect(this, SIGNAL(startLayoutCalcSignal()), mGrapher, SLOT(layoutNodes()),
          Qt::QueuedConnection);

  connect(mGrapher, SIGNAL(initReadySignal()), this,
          SLOT(graphInitReadySlot()));

  qRegisterMetaType<TestDataGen::testcases>("TestDataGen::testcases");

  connect(this, &App::startTestCaseSignal, mTestData,
          &TestDataGen::StartTestCaseSlot, Qt::QueuedConnection);

  connect(this, SIGNAL(stopTestCaseSignal()), mTestData,
          SLOT(StopTestCaseExecutionSlot()), Qt::QueuedConnection);

  mAccess->startProcessing(QThread::currentThread());
  mGrapher->startProcessing(QThread::currentThread());

  setProcessedmessagescount(mProcessedMsgsCounter);

  gApp = this; // singleton. QML Creates App object, so make it accessible for
               // C++ here
}

QGraphicsScene *App::scene() const { return mScene; }

void App::updateCoordsSlot(QHash<QString, NodeItem> *coords) {
  TTPLOG_FUNCTIONSCOPE;

  /* Update node coords from graph */
  if (coords != NULL) {

    if (coords->size() > 0) {

      qreal xAvg = 0;
      qreal yAvg = 0;
      foreach (NodeItem n, *coords) {
        xAvg += n.x;
        yAvg += n.y;
      }
      xAvg /= (qreal)coords->size();
      yAvg /= (qreal)coords->size();

      qreal offsetX;
      qreal offsetY;
#if 0
            offsetX = mView->sceneRect().center().x()-xAvg;
            offsetY = mView->sceneRect().center().y()-yAvg;
#else
      offsetX = 0;
      offsetY = 0;
#endif

      foreach (NodeItem n, *coords) {

        if (mGraphNodes->contains(n.label) == true) {
          GraphNode *myNode = mGraphNodes->value(n.label);
          myNode->TargetCoordX = n.x + offsetX;
          myNode->TargetCoordY = n.y + offsetY;
        } else {
          // Q_ASSERT(0);
        }
      }
    }

    if (mFastAutoLayoutReg == true) {
      calculateFastLayoutToCoords();
      mFastAutoLayoutReg = false;
    }

    delete coords;
  }
}

void App::graphInitReadySlot() {
  TTPLOG_FUNCTIONSCOPE;

  TTPLOG_LOG_OS_COUNTERS;

  connect(&mAnimationtime, SIGNAL(timeout()), this, SLOT(animatorTimeOut()));
  mAnimationtime.start(1000.0 / 20.0);

  mReLayoutTimer.restart();

  mView->SetCurrentNodeList(mGraphNodes);
  mView->SetCurrentLinkList(mGraphLinks);

#ifdef USETESTDATAGENERATOR
  QTimer::singleShot(1000, this, SLOT(startTestDataGenerator()));
#endif
}

void App::setParseproblemscounter(int parseproblemscounter) {
  if (m_parseproblemscounter == parseproblemscounter)
    return;

  m_parseproblemscounter = parseproblemscounter;
  emit parseproblemscounterChanged(parseproblemscounter);
}

void App::connectionStatusChanged(QString connectionStatus) {
  setConnectionstatus(connectionStatus);
}

void App::setConnectionstatus(QString connectionstatus) {
  if (m_connectionstatus == connectionstatus)
    return;

  m_connectionstatus = connectionstatus;
  emit connectionstatusChanged(connectionstatus);
}

void App::setNodecount(int nodecount) {
  if (m_nodecount == nodecount)
    return;

  m_nodecount = nodecount;
  emit nodecountChanged(nodecount);
}

void App::setProcessedmessagescount(quint64 processedmessagescount) {
  if (m_processedmessagescount == processedmessagescount)
    return;

  m_processedmessagescount = processedmessagescount;
  emit processedmessagescountChanged(processedmessagescount);
}

void App::setDisableCoordinates(bool disableCoordinates) {
  if (m_disableCoordinates == disableCoordinates)
    return;

  m_disableCoordinates = disableCoordinates;
  emit disableCoordinatesChanged(disableCoordinates);

  if (m_disableCoordinates == true) {
    /* perform autolayout now */

    /* Set Proper image */

    QStringList emptyFilename;
    emptyFilename.append(QString(""));
    hncLoadImage(emptyFilename);

    doLayout();
    mFastAutoLayoutReg =
        true; // we can perform fast layout once graph result is available.

  } else {
    /* Recall proper image */
    if (mLatestBackGroundImageFilename.length() > 0) {
      QStringList fileName;
      fileName.append(mLatestBackGroundImageFilename);
      hncLoadImage(fileName);
    }

    reApplyFixedCoordsToNodes();
    calculateFastLayoutToCoords();
  }
}

NodeGraphicsView *App::view() const { return mView; }

void App::startTestCase(int caseNumber) {
  QStringList fileName;
  fileName.append("");
  hncLoadImage(fileName);
  mLatestBackGroundImageFilename = "";

  TestDataGen::testcases testCase =
      static_cast<TestDataGen::testcases>(caseNumber);

  emit startTestCaseSignal(testCase);
  mDemosRunning = true;
}

void App::stopTestCases() {
  emit stopTestCaseSignal();
  mDemosRunning = false;
  hncClear();
}

void App::mousePos(int screenX, int screenY) {}

void App::updateNodesAndLinks() {
  TTPLOG_FUNCTIONSCOPE;

  /* Calculate max values */
  qreal mNodesCurrentMaxValue = 0;
  qreal mLinksCurrentMaxValue = 0;

  if (mGraphLinks != NULL) {
    foreach (GraphLink *l, *mGraphLinks) {
      mLinksCurrentMaxValue = qMax(mLinksCurrentMaxValue, l->linkValue());
    }

    foreach (GraphLink *l, *mGraphLinks) {
      if (mLinksCurrentMaxValue > 0.0) {
        l->setLinkValueNormalizedToMaxLinkValue(l->linkValue() /
                                                mLinksCurrentMaxValue);
      }

      l->updateLink(!m_disableCoordinates, mColorsCalculator);
    }
  }

  /* Update */
  if (mGraphNodes != NULL) {

    foreach (GraphNode *p, *mGraphNodes) {
      mNodesCurrentMaxValue = qMax(mNodesCurrentMaxValue, p->mValue);
    }

    foreach (GraphNode *p, *mGraphNodes) {
      if (mNodesCurrentMaxValue > 0.0) {
        p->setNodeValueNormalizedToMaxNodeValue(p->mValue /
                                                mNodesCurrentMaxValue);
      }
      p->updateNode(!m_disableCoordinates, mColorsCalculator);
    }
  }
}

void App::doLayout() {
  TTPLOG_FUNCTIONSCOPE;
  emit startLayoutCalcSignal();
  mReLayoutTimer.restart();
}

void App::animatorTimeOut() {
  TTPLOG_FUNCTIONSCOPE;
  updateNodesAndLinks();

  if (mGraphNodes != NULL) {
    setNodecount(mGraphNodes->size());
  }
  /* Increase process counter */
  setProcessedmessagescount(mProcessedMsgsCounter);

  mScene->invalidate(mScene->sceneRect());
  mView->RenderToCacheImage();
  emit animationReqSignal();

  TTPLOG_VALUE("items.scene.count", mScene->items().count());

  if (mAutoLayoutEnabled == true || m_disableCoordinates == true) {
    const int defaultrelayoutInterValMs = 1000;
    if (mReLayoutTimer.elapsed() > defaultrelayoutInterValMs) {
      if (m_disableCoordinates == true) {
        doLayout();
      }
    }
  }
}

void App::hncAddlink(QStringList messageParts, bool &relayoutGraph) {
  TTPLOG_FUNCTIONSCOPE;
  if (messageParts.size() == 2) {
    QString nodeLabel1 = messageParts.takeFirst();
    QString nodeLabel2 = messageParts.takeFirst();
    addLink(nodeLabel1, nodeLabel2, buildLinkName(nodeLabel1, nodeLabel2));
    mGrapher->AddLink(nodeLabel1, nodeLabel2);

    relayoutGraph = true;
  } else {
    mMessageParserProblemCounter++;
    setParseproblemscounter(mMessageParserProblemCounter);
  }
}

void App::hncRemoveLink(QStringList messageParts, bool &relayoutGraph) {
  TTPLOG_FUNCTIONSCOPE;
  if (messageParts.size() == 2) {
    QString nodeLabel1 = messageParts.takeFirst();
    QString nodeLabel2 = messageParts.takeFirst();
    removeLink(nodeLabel1, nodeLabel2, buildLinkName(nodeLabel1, nodeLabel2));
    mGrapher->RemoveLink(nodeLabel1, nodeLabel2);
    relayoutGraph = true;
  } else {
    mMessageParserProblemCounter++;
    setParseproblemscounter(mMessageParserProblemCounter);
  }
}

void App::removeAllNodes() {
  QList<QString> k = mGraphNodes->keys();
  foreach (QString key, k) {
    GraphNode *n = mGraphNodes->value(key);
    if (n->mVisualNode != NULL) {
      mScene->removeItem(n->mVisualNode);
      delete n->mVisualNode;
      n->mVisualNode = NULL;
    }

    delete n;
  }
  mGraphNodes->clear();
  Q_ASSERT(mGraphNodes->size() == 0);
}

void App::removeAllLinks() {
  QList<QString> l = mGraphLinks->keys();
  foreach (QString key, l) {

    GraphLink *g = mGraphLinks->value(key);
    delete g;
  }
  mGraphLinks->clear();

  Q_ASSERT(mGraphLinks->size() == 0);
}

void App::hncClear() {
  TTPLOG_FUNCTIONSCOPE;
  mGrapher->ClearNodes();
  removeAllNodes();
  removeAllLinks();
  QStringList emptyFilename;
  emptyFilename.append(QString(""));
  hncLoadImage(emptyFilename);
  doLayout();
}

void App::hncCreateNode(QStringList messageParts, bool &relayoutGraph) {
  TTPLOG_FUNCTIONSCOPE;
  if (messageParts.size() == 1) {
    QString nodeLabel = messageParts.takeFirst();
    mGrapher->AddNewNode(nodeLabel);
    addNode(nodeLabel);
    relayoutGraph = true;
  } else {
    mMessageParserProblemCounter++;
    setParseproblemscounter(mMessageParserProblemCounter);
  }
}

void App::hncColorLink(QStringList messageParts) {
#if 1
  TTPLOG_FUNCTIONSCOPE;

  if (messageParts.size() == 3) {
    QString nodeLabel1 = messageParts.takeFirst();
    QString nodeLabel2 = messageParts.takeFirst();
    QString namedColor = messageParts.takeFirst();

    QString linkName = buildLinkName(nodeLabel1, nodeLabel2);

    if (mGraphLinks->contains(linkName) == true) {
      mGraphLinks->value(linkName)->setLinkColor(namedColor);
    }
  } else {
    mMessageParserProblemCounter++;
    setParseproblemscounter(mMessageParserProblemCounter);
  }
#endif
}

void App::hncColorNode(QStringList messageParts) {
  if (messageParts.size() == 2) {
    QString nodeLabel = messageParts.takeFirst();
    QString nodeNamedColor = messageParts.takeFirst();
    if (mGraphNodes->contains(nodeLabel) == true) {
      mGraphNodes->value(nodeLabel)->setNodeColor(nodeNamedColor);
    }
  } else {
    mMessageParserProblemCounter++;
    setParseproblemscounter(mMessageParserProblemCounter);
  }
}

void App::hncRemoveNode(QStringList messageParts, bool &relayoutGraph) {
  TTPLOG_FUNCTIONSCOPE;
  if (messageParts.size() == 1) {
    QString nodeLabel = messageParts.takeFirst();
    mGrapher->RemoveNode(nodeLabel);
    removeNode(nodeLabel);

    relayoutGraph = true;
  } else {
    mMessageParserProblemCounter++;
    setParseproblemscounter(mMessageParserProblemCounter);
  }
}

void App::hncLoadImage(QStringList messageParts) {
  if (messageParts.size() == 1) {
    QString fileNameWithPath = messageParts.takeFirst();

    if (m_disableCoordinates == false) {
      if (fileNameWithPath.length() > 0) {
        mLatestBackGroundImageFilename = fileNameWithPath;
      }
    } else {

      // Do not use but store
      if (fileNameWithPath.length() > 0) {
        mLatestBackGroundImageFilename = fileNameWithPath;
      }
    }

    QRectF imageRect = mView->LoadBackGroundImage(fileNameWithPath);
    QRect r;

    if (imageRect.width() > 0 && imageRect.height() > 0) {
      r = imageRect.toRect();
      mView->setSceneRect(imageRect);
    } else {
      mView->setSceneRect(defaultScene);
      r = QRect(0, 0, 0, 0);
    }

    emit sceneSizeChangeReqSignal(r);

  } else {
    mMessageParserProblemCounter++;
    setParseproblemscounter(mMessageParserProblemCounter);
  }
}

void App::setNodeCoord(QString nodeLabel, qreal x, qreal y,
                       bool bStoreCoordPositionOnly) {
  if (mGraphNodes->contains(nodeLabel) == true) {

    mGraphNodes->value(nodeLabel)->LastSetCoordX = x;
    mGraphNodes->value(nodeLabel)->LastSetCoordY = y;

    if (bStoreCoordPositionOnly == false) {

      mGraphNodes->value(nodeLabel)->TargetCoordX = x;
      mGraphNodes->value(nodeLabel)->TargetCoordY = y;

      mGraphNodes->value(nodeLabel)->CurrentCoordX =
          mGraphNodes->value(nodeLabel)->TargetCoordX;
      mGraphNodes->value(nodeLabel)->CurrentCoordY =
          mGraphNodes->value(nodeLabel)->TargetCoordY;
    }
  }
}

void App::reApplyFixedCoordsToNodes() {
  foreach (GraphNode *m, *mGraphNodes) {
    m->TargetCoordX = m->LastSetCoordX;
    m->TargetCoordY = m->LastSetCoordY;
  }
}

void App::calculateFastLayoutToCoords() {
  foreach (GraphNode *m, *mGraphNodes) {
    m->CurrentCoordX = m->TargetCoordX;
    m->CurrentCoordY = m->TargetCoordY;
  }
}

int App::parseproblemscounter() const { return m_parseproblemscounter; }

QString App::connectionstatus() const { return m_connectionstatus; }

int App::nodecount() const { return m_nodecount; }

quint64 App::processedmessagescount() const { return m_processedmessagescount; }

bool App::disableCoordinates() const { return m_disableCoordinates; }

void App::hncSetCoord(QStringList messageParts) {
  TTPLOG_FUNCTIONSCOPE;
  if (messageParts.size() == 3) {
    QString nodeLabel = messageParts.takeFirst();
    QString x = messageParts.takeFirst();
    QString y = messageParts.takeFirst();

    if (m_disableCoordinates == false) {
      mGrapher->SetCoord(nodeLabel, x.toDouble(), y.toDouble());
      setNodeCoord(nodeLabel, x.toDouble(), y.toDouble(), false);

      // When applying manual coordinates, no autolayout after that
      mAutoLayoutEnabled = false;

    } else {
      /* User has disabled coordinates from UI. Do not update them until they
       * are enabled again */
      setNodeCoord(nodeLabel, x.toDouble(), y.toDouble(), true);
      mAutoLayoutEnabled = true;
    }
  } else {
    mMessageParserProblemCounter++;
    setParseproblemscounter(mMessageParserProblemCounter);
  }
}

void App::hncSetLinkValue(QStringList messageParts) {
  // TTPLOG_STRING(QString("nodes;setlinkvalue;%1;%2;%3");
  if (messageParts.size() == 3) {
    QString nodeLabel1 = messageParts.takeFirst();
    QString nodeLabel2 = messageParts.takeFirst();
    QString value = messageParts.takeFirst();
    bool bOk;
    qreal linkValue = value.toDouble(&bOk);
    if (bOk == true) {

      if (linkValue >= 0) {
        QString linkName = buildLinkName(nodeLabel1, nodeLabel2);
        if (mGraphLinks->contains(linkName) == true) {
          mGraphLinks->value(linkName)->setLinkValue(linkValue);

          /* Add link value also to graph since is used for layout */
          mGrapher->SetLinkValue(nodeLabel1, nodeLabel2, linkValue);
        }
      } else {
        mMessageParserProblemCounter++;
        setParseproblemscounter(mMessageParserProblemCounter);
      }
    } else {
      mMessageParserProblemCounter++;
      setParseproblemscounter(mMessageParserProblemCounter);
    }
  } else {
    mMessageParserProblemCounter++;
    setParseproblemscounter(mMessageParserProblemCounter);
  }
}

void App::hncNodeValue(QStringList messageParts) {
  // TTPLOG_STRING(QString("nodes;setnodevalue;%1;%2");
  if (messageParts.size() == 2) {
    QString nodeLabel = messageParts.takeFirst();
    QString value = messageParts.takeFirst();
    bool bOk;
    qreal nodeValue = value.toDouble(&bOk);
    if (bOk == true) {
      if (nodeValue >= 0) {

        if (mGraphNodes->contains(nodeLabel) == true) {
          mGraphNodes->value(nodeLabel)->setNodeValue(nodeValue);
        }

      } else {
        mMessageParserProblemCounter++;
        setParseproblemscounter(mMessageParserProblemCounter);
      }
    } else {
      mMessageParserProblemCounter++;
      setParseproblemscounter(mMessageParserProblemCounter);
    }
  } else {
    mMessageParserProblemCounter++;
    setParseproblemscounter(mMessageParserProblemCounter);
  }
}

void App::hncNodeSize(QStringList messageParts) {
  // TTPLOG_STRING(QString("nodes;setnodesize;%1;%2");
  if (messageParts.size() == 2) {
    QString nodeLabel = messageParts.takeFirst();
    QString value = messageParts.takeFirst();
    bool bOk;
    qreal nodeSize = value.toDouble(&bOk);
    if (bOk == true) {
      if (nodeSize >= 0) {

        if (mGraphNodes->contains(nodeLabel) == true) {
          mGraphNodes->value(nodeLabel)->setNodeSize(nodeSize);
        }

      } else {
        mMessageParserProblemCounter++;
        setParseproblemscounter(mMessageParserProblemCounter);
      }
    } else {
      mMessageParserProblemCounter++;
      setParseproblemscounter(mMessageParserProblemCounter);
    }

  } else {
    mMessageParserProblemCounter++;
    setParseproblemscounter(mMessageParserProblemCounter);
  }
}

void App::hncNodeLabel(QStringList messageParts) {
  // TTPLOG_STRING(QString("nodes;setnodelabel;%1;%2");
  if (messageParts.size() == 2) {
    QString nodeLabel = messageParts.takeFirst();
    QString nodeText = messageParts.takeFirst();

    if (mGraphNodes->contains(nodeLabel) == true) {
      mGraphNodes->value(nodeLabel)->setNodeLabel(nodeText);
    }
  } else {
    mMessageParserProblemCounter++;
    setParseproblemscounter(mMessageParserProblemCounter);
  }
}

void App::handleNetworkCommandSlot(QStringList messageParts) {
  TTPLOG_FUNCTIONSCOPE;

  bool relayoutGraph = false;
  /* Process messages and do needed tricks */
  if (messageParts.size() > 0) {
    QString cmd = messageParts.takeFirst();

    if (cmd == "addlink") {
      // TTPLOG_STRING(QString("nodes;addlink;%1;%2").arg((int)myItem).arg((int)myCand));
      hncAddlink(messageParts, relayoutGraph);
    } else if (cmd == "clear") {
      hncClear();
    } else if (cmd == "create") {
      // TTPLOG_STRING(QString("nodes;create;%1").arg((int)p));
      hncCreateNode(messageParts, relayoutGraph);
    } else if (cmd == "disableautolayout") {
      // TTPLOG_STRING(QString("nodes;disableautolayout");
      mAutoLayoutEnabled = false;
    } else if (cmd == "enableautolayout") {
      // TTPLOG_STRING(QString("nodes;enableautolayout");
      mAutoLayoutEnabled = true;
    } else if (cmd == "loadBackGroundImage") {
      // TTPLOG_STRING(QString("nodes;loadBackGroundImage;%1").arg(QString
      // pathToImage);
      hncLoadImage(messageParts);
    } else if (cmd == "setcoord") {
      /*TTPLOG_STRING(QString("nodes;setcoord;%1;%2;%3").arg((int)p)
                    .arg(QRect(x,y,edge,edge).center().x())
                    .arg(QRect(x,y,edge,edge).center().y()));*/

      hncSetCoord(messageParts);

    } else if (cmd == "removelink") {
      // TTPLOG_STRING(QString("nodes;removelink;%1;%2").arg(label1).arg(label2));
      hncRemoveLink(messageParts, relayoutGraph);
    } else if (cmd == "removenode") {
      // TTPLOG_STRING(QString("nodes;removenode;%1;%2").arg(label1).arg(label2));
      hncRemoveNode(messageParts, relayoutGraph);
    } else if (cmd == "relayout") {
      doLayout();
    } else if (cmd == "setlinkcolor") {
      // TTPLOG_STRING(QString("nodes;setlinkcolor;%1;%2;%3");
      hncColorLink(messageParts);
    } else if (cmd == "setnodecolor") {
      // TTPLOG_STRING(QString("nodes;setnodecolor;%1;%2");
      hncColorNode(messageParts);
    } else if (cmd == "setlinkvalue") {
      // TTPLOG_STRING(QString("nodes;setlinkvalue;%1;%2;%3");
      hncSetLinkValue(messageParts);
    } else if (cmd == "setnodevalue") {
      // TTPLOG_STRING(QString("nodes;setnodevalue;%1;%2");
      hncNodeValue(messageParts);
    } else if (cmd == "setnodesize") {
      // TTPLOG_STRING(QString("nodes;setnodesize;%1;%2");
      hncNodeSize(messageParts);
    } else if (cmd == "setnodelabel") {
      // TTPLOG_STRING(QString("nodes;setnodelabel;%1;%2");
      hncNodeLabel(messageParts);
    } else {
      /* unknown command */
    }
  }
  mProcessedMsgsCounter++;
}

void App::startTestDataGenerator() {
  TTPLOG_FUNCTIONSCOPE;
  mTestData->startProcessing(QThread::currentThread());
  emit startLayoutCalcSignal();
}

QString App::buildLinkName(QString label1, QString label2) {
  return QString("%1%2").arg(label1).arg(label2);
}

GraphNode *App::addNode(QString label) {
  GraphNode *ret = NULL;

  if (mGraphNodes->contains(label) == false) {
    ret = new GraphNode;
    ret->mNodeName = label;
    GraphicsNodeItem *gN = new GraphicsNodeItem();
    gN->setNamedColorName("red");
    gN->setDiameter(ret->mDiameter);
    gN->setNodeName(label);

    ret->mVisualNode = gN;
    mGraphNodes->insert(label, ret);
    mScene->addItem(gN);
  } else {
    ret = mGraphNodes->value(label);
  }
  return ret;
}

GraphLink *App::addLink(QString label1, QString label2, QString linkName) {
  GraphLink *ret = NULL;

  if (mGraphNodes->contains(label1) == true &&
      mGraphNodes->contains(label2) == true) {

    if (mGraphNodes->value(label1)->linkedNodes.contains(label2) == false) {
      mGraphNodes->value(label1)->linkedNodes.insert(label2, label2);
    }

    if (mGraphLinks->contains(linkName) == false) {
      ret = new GraphLink;
      ret->mLinkName = linkName;
      mGraphLinks->insert(linkName, ret);
    } else {
      ret = mGraphLinks->value(linkName);
    }
  }
  return ret;
}

void App::removeLink(QString label1, QString label2, QString linkName) {
  if (mGraphNodes->contains(label1) == true &&
      mGraphNodes->contains(label2) == true) {

    if (mGraphNodes->value(label1)->linkedNodes.contains(label2) == true) {
      mGraphNodes->value(label1)->linkedNodes.remove(label2);
    }

    if (mGraphLinks->contains(linkName) == true) {
      GraphLink *p = mGraphLinks->value(linkName);
      delete p;
      mGraphLinks->remove(linkName);
    }
  }
}

void App::removeNode(QString label) {
  if (mGraphNodes->contains(label) == true) {
    GraphNode *p = mGraphNodes->value(label);
    if (p->mVisualNode != NULL) {
      mScene->removeItem(p->mVisualNode);
      delete p->mVisualNode;
      p->mVisualNode = NULL;
    }
    delete p;
    mGraphNodes->remove(label);
  }
}

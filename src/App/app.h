#ifndef APP_H
#define APP_H

#include <QElapsedTimer>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QList>
#include <QObject>
#include <QTimer>

#include <ogdf/basic/Graph.h>
#include <ogdf/basic/graph_generators.h>
#include <ogdf/fileformats/GraphIO.h>
#include <ogdf/layered/DfsAcyclicSubgraph.h>

#include "colorgenerator.h"
#include "graphlink.h"
#include "graphnode.h"
#include "graphs.h"
#include "networkaccessandapi.h"
#include "nodegraphicsview.h"
#include "nodescene.h"
#include "testdatagen.h"

#define USETESTDATAGENERATOR // comment to disable

class App : public QObject {
  Q_OBJECT
public:
  explicit App(QObject *parent = 0);
  static App *Instance();
  QGraphicsScene *scene() const;
  NodeGraphicsView *view() const;

  Q_PROPERTY(int parseproblemscounter READ parseproblemscounter WRITE
                 setParseproblemscounter NOTIFY parseproblemscounterChanged)
  Q_PROPERTY(QString connectionstatus READ connectionstatus WRITE
                 setConnectionstatus NOTIFY connectionstatusChanged)
  Q_PROPERTY(
      int nodecount READ nodecount WRITE setNodecount NOTIFY nodecountChanged)
  Q_PROPERTY(quint64 processedmessagescount READ processedmessagescount WRITE
                 setProcessedmessagescount NOTIFY processedmessagescountChanged)
  Q_PROPERTY(bool disableCoordinates READ disableCoordinates WRITE
                 setDisableCoordinates NOTIFY disableCoordinatesChanged)

  Q_INVOKABLE void startTestCase(int caseNumber);
  Q_INVOKABLE void stopTestCases();
  Q_INVOKABLE void mousePos(int screenX, int screenY);

  int parseproblemscounter() const;

  QString connectionstatus() const;

  bool disableCoordinates() const;

  int nodecount() const;

  quint64 processedmessagescount() const;

signals:
  void animationReqSignal();

  void connectionstatusChanged(QString connectionstatus);

  void disableCoordinatesChanged(bool disableCoordinates);

  void nodecountChanged(int nodecount);

  void parseproblemscounterChanged(int parseproblemscounter);

  void processedmessagescountChanged(quint64 processedmessagescount);

  void startLayoutCalcSignal();

  void sceneSizeChangeReqSignal(QRectF r);

  void startTestCaseSignal(TestDataGen::testcases caseNumber);
  void stopTestCaseSignal();

public slots:
  void animatorTimeOut();

  void connectionStatusChanged(QString connectionStatus);

  void graphInitReadySlot();

  void handleNetworkCommandSlot(QStringList messageParts);

  void startTestDataGenerator();
  void setParseproblemscounter(int parseproblemscounter);
  void setConnectionstatus(QString connectionstatus);
  void setNodecount(int nodecount);
  void setProcessedmessagescount(quint64 processedmessagescount);
  void setDisableCoordinates(bool disableCoordinates);

  void updateCoordsSlot(QHash<QString, NodeItem> *coords);

private:
  GraphNode *addNode(QString label);
  GraphLink *addLink(QString label1, QString label2, QString linkName);

  QString buildLinkName(QString label1, QString label2);

  void doLayout();

  void hncAddlink(QStringList messageParts, bool &relayoutGraph);
  void hncRemoveLink(QStringList messageParts, bool &relayoutGraph);
  void hncClear();
  void hncCreateNode(QStringList messageParts, bool &relayoutGraph);
  void hncColorLink(QStringList messageParts);
  void hncColorNode(QStringList messageParts);
  void hncRemoveNode(QStringList messageParts, bool &relayoutGraph);
  void hncLoadImage(QStringList messageParts);
  void hncSetCoord(QStringList messageParts);
  void hncSetLinkValue(QStringList messageParts);
  void hncNodeValue(QStringList messageParts);
  void hncNodeSize(QStringList messageParts);
  void hncNodeLabel(QStringList messageParts);

  void removeNode(QString label);
  void removeLink(QString label1, QString label2, QString linkName);
  void removeAllNodes();
  void removeAllLinks();

  void setNodeCoord(QString nodeLabel, qreal x, qreal y,
                    bool bStoreCoordPositionOnly);
  void reApplyFixedCoordsToNodes();
  void calculateFastLayoutToCoords();

  void updateNodesAndLinks();

  NodeScene *mScene;
  NodeGraphicsView *mView;
  QTimer mAnimationtime;

  QElapsedTimer mReLayoutTimer;

  QHash<QString, GraphNode *> *mGraphNodes;
  QHash<QString, GraphLink *> *mGraphLinks;

  NetworkAccessAndAPI *mAccess;
  Graphs *mGrapher;
  TestDataGen *mTestData;
  ColorGenerator mColorsCalculator;

  bool mFastAutoLayoutReg;
  bool mDemosRunning;
  bool m_disableCoordinates;
  bool mAutoLayoutEnabled;
  quint64 mMessageParserProblemCounter;
  quint64 mProcessedMsgsCounter;
  quint64 m_processedmessagescount;
  int m_parseproblemscounter;
  int m_nodecount;
  QString m_connectionstatus;
  QString mLatestBackGroundImageFilename;
};

#endif // APP_H

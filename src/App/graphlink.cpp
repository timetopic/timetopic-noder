/*

TimeToPic Noder visualizes networks real time. Tool for fast prototyping.

Copyright (c) 2015 Herwood Technologies Oy,
Erkki Salonen erkki@herwoodtechnologies.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


#include "graphlink.h"

GraphLink::GraphLink() : mLinkValue(1) , mLinkColor(""), mLinkValueNormalizedToMaxLinkValue(0)
{

}
QString GraphLink::linkColor() const
{
    return mLinkColor;
}

void GraphLink::setLinkColor(const QString &linkColor)
{
    mLinkColor = linkColor;
}
qreal GraphLink::linkValueNormalizedToMaxLinkValue() const
{
    return mLinkValueNormalizedToMaxLinkValue;
}

void GraphLink::setLinkValueNormalizedToMaxLinkValue(const qreal &linkValueNormalizedToMaxLinkValue)
{
    mLinkValueNormalizedToMaxLinkValue = linkValueNormalizedToMaxLinkValue;
}

void GraphLink::updateLink(bool useFixedCoords, ColorGenerator &colorGen)
{
    Q_UNUSED(useFixedCoords);
    if (this->mLinkColor.length()==0) {
        /* No color specified. Try use value */
        QColor myColor = colorGen.getColor(mLinkValueNormalizedToMaxLinkValue);
        mLinkVisualizationColor = myColor.name();
    }
    else {
        mLinkVisualizationColor = this->mLinkColor;
    }
}
QString GraphLink::linkVisualizationColor() const
{
    return mLinkVisualizationColor;
}



qreal GraphLink::linkValue() const
{
    return mLinkValue;
}

void GraphLink::setLinkValue(const qreal &linkValue)
{

    /* Nodevalue is being transformed to color */
    mLinkValue = linkValue;
}



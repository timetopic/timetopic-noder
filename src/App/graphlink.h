#ifndef GRAPHLINK_H
#define GRAPHLINK_H

#include <QString>
#include <QColor>
#include "colorgenerator.h"

class GraphLink
{
public:
    GraphLink();
    QString mLinkName;

    qreal linkValue() const;
    void setLinkValue(const qreal &linkValue);
    QString linkColor() const;
    void setLinkColor(const QString &linkColor);
    qreal linkValueNormalizedToMaxLinkValue() const;
    void setLinkValueNormalizedToMaxLinkValue(const qreal &linkValueNormalizedToMaxLinkValue);

    void  updateLink(bool useFixedCoords, ColorGenerator &colorGen);

    QString linkVisualizationColor() const;

private:
    qreal mLinkValue;
    qreal mLinkValueNormalizedToMaxLinkValue;
    QString mLinkColor;
    QString mLinkVisualizationColor;
};

#endif // GRAPHLINK_H

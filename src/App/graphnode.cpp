/*

TimeToPic Noder visualizes networks real time. Tool for fast prototyping.

Copyright (c) 2015 Herwood Technologies Oy,
Erkki Salonen erkki@herwoodtechnologies.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


#include "graphnode.h"
#include <QLine>

const qreal defaultSpeed = 30.0;

GraphNode::GraphNode() : mVisualNode(NULL), mNodeValueNormalizedToMaxNodeValue(0)
{
    CurrentCoordX =0;
    CurrentCoordY =0;
    LastSetCoordX =-1;
    LastSetCoordY =-1;

    mValue = 1.0; // default value
    mDiameter = 1.0; // default size

    speed = defaultSpeed;
}

GraphNode::~GraphNode()
{

}

void GraphNode::updateNode(bool useFixedCoords,ColorGenerator &colorGen)
{
    if (useFixedCoords==true) {
        if (TargetCoordX!=LastSetCoordX && LastSetCoordX!=-1) {
            TargetCoordX=LastSetCoordX;
        }
        if (TargetCoordY!=LastSetCoordY && LastSetCoordY!=-1) {
            TargetCoordY=LastSetCoordY;
        }
    }

    QLineF vect(QPointF(CurrentCoordX,CurrentCoordY),
                QPointF(TargetCoordX,TargetCoordY));

    if (vect.length()>defaultSpeed) {

        qreal f = speed/vect.length();

        qreal dx = vect.dx()*f;
        qreal dy = vect.dy()*f;

        CurrentCoordX+=dx;
        CurrentCoordY+=dy;     
    }

    if (mVisualNode!=NULL) {
        mVisualNode->setX(CurrentCoordX);
        mVisualNode->setY(CurrentCoordY);

        /* Update possible size changes */
        mVisualNode->setDiameter(mDiameter);

        if (this->mNodeColor.length()==0) {
            /* No color specified. Try use value */
            QColor myColor = colorGen.getColor(this->nodeValueNormalizedToMaxNodeValue());
            mVisualNode->setNamedColorName(myColor.name());
        }
    }
}

void GraphNode::setNodeColor(QString NamedColorName)
{
    mNodeColor = NamedColorName;
    if (mVisualNode!=NULL) {
        mVisualNode->setNamedColorName(mNodeColor);
    }
}

void GraphNode::setNodeValue(qreal value)
{
    /* Nodevalue is being transformed to color */
    mValue = value;
}

void GraphNode::setNodeSize(qreal diameter)
{
    mDiameter = diameter;
    if (mVisualNode!=NULL) {
        mVisualNode->setDiameter(mDiameter);
    }
}

void GraphNode::setNodeLabel(QString label)
{
    mNodeLabel = label;
    if (mVisualNode!=NULL) {
        mVisualNode->setLabel(mNodeLabel);
    }

}
qreal GraphNode::nodeValueNormalizedToMaxNodeValue() const
{
    return mNodeValueNormalizedToMaxNodeValue;
}

void GraphNode::setNodeValueNormalizedToMaxNodeValue(const qreal &nodeValueNormalizedToMaxNodeValue)
{
    mNodeValueNormalizedToMaxNodeValue = nodeValueNormalizedToMaxNodeValue;
}



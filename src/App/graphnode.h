#ifndef GRAPHNODE_H
#define GRAPHNODE_H

#include <QString>
#include <Qlist>
#include <QHash>
#include "graphicsnodeitem.h"
#include "colorgenerator.h"

class GraphNode
{
public:

    GraphNode();
    ~GraphNode();
    void  updateNode(bool useFixedCoords, ColorGenerator &colorGen);
    void  setNodeColor(QString NamedColorName);
    void  setNodeValue(qreal mValue);
    void  setNodeSize(qreal mDiameter);
    void  setNodeLabel(QString label);
    QString mNodeName;
    GraphicsNodeItem *mVisualNode;
    qreal TargetCoordX;
    qreal TargetCoordY;
    qreal LastSetCoordX; // if node has set value, its last value is stored here
    qreal LastSetCoordY; // if node has set value, its last value is stored here

    QString mNodeLabel;

    qreal CurrentCoordX;
    qreal CurrentCoordY;
    qreal speed;

    QString mNodeColor;

    qreal mValue;
    qreal mNodeValueNormalizedToMaxNodeValue;
    qreal mDiameter;
    QHash<QString,QString> linkedNodes;    
    qreal nodeValueNormalizedToMaxNodeValue() const;
    void setNodeValueNormalizedToMaxNodeValue(const qreal &nodeValueNormalizedToMaxNodeValue);
};

#endif // GRAPHNODE_H

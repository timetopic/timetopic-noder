/*

TimeToPic Noder visualizes networks real time. Tool for fast prototyping.

Copyright (c) 2015 Herwood Technologies Oy,
Erkki Salonen erkki@herwoodtechnologies.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "graphs.h"
#include "timetopiclogger.h"
#include <QDebug>
#include <QElapsedTimer>
#include <QMutex>
#include <QMutexLocker>
#include <QtGlobal>
#include <list>

void Graphs::Init() {
  if (mG != NULL) {
    delete mG;
  }
  if (mGA != NULL) {
    delete mGA;
  }

  mG = new EdgeWeightedGraph<int>;
  mGA = new GraphAttributes;
  mGA->init(*mG, GraphAttributes::nodeGraphics | GraphAttributes::edgeGraphics |
                     GraphAttributes::nodeLabel | GraphAttributes::nodeWeight |
                     GraphAttributes::edgeLabel);
}

void Graphs::InitGraph() {
  QMutexLocker locker(mAccessLock);
  Init();
}

Graphs::Graphs()
    : mG(NULL), mGA(NULL), NodeLookup(NULL), EdgeLookup(NULL),
      mAccessLock(NULL), mTodoList(NULL) {}

void Graphs::InitThis() {
  TTPLOG_FUNCTIONSCOPE;
  NodeLookup = new QHash<QString, node>;
  EdgeLookup = new QHash<QString, edge>;
  mAccessLock = new QMutex;
  mTodoList = new QList<todo>;
  mProcessTodoListTimer = new QTimer;

  InitGraph();

  connect(this, SIGNAL(ProcessTodoListSignal()), this, SLOT(ProcessTodoList()),
          Qt::QueuedConnection);

  connect(mProcessTodoListTimer, SIGNAL(timeout()), this,
          SLOT(ProcessTodoList()));
  mProcessTodoListTimer->start(100);
  emit initReadySignal();
}

void Graphs::AddNewNode(QString label) {
  TTPLOG_FUNCTIONSCOPE;
  todo t;
  t.cmd = __FUNCTION__;
  t.label1 = label;

  {
    QMutexLocker locker(mAccessLock);
    mTodoList->append(t);
  }
}

void Graphs::ProcessAddNewNode(QString label) {
  if (NodeLookup != NULL && NodeLookup->contains(label) == false) {

    node myNode = mG->newNode();
    std::string myStr = label.toStdString();
    mGA->label(myNode) = myStr;
    const int defaultEdge = 10;
    mGA->width(myNode) = defaultEdge;
    mGA->height(myNode) = defaultEdge;
    NodeLookup->insert(label, myNode);
  }
}

void Graphs::RemoveNode(QString label) {
  TTPLOG_FUNCTIONSCOPE;

  todo t;
  t.cmd = __FUNCTION__;
  t.label1 = label;

  {
    QMutexLocker locker(mAccessLock);
    mTodoList->append(t);
  }
}

QString Graphs::NodeEdgeLabel(QString label1, QString label2) {
  QString nodeLookupLabel = QString("l-%1-%2").arg(label1).arg(label2);

  return nodeLookupLabel;
}

void Graphs::AddLink(QString label1, QString label2) {
  TTPLOG_FUNCTIONSCOPE;

  todo t;
  t.cmd = __FUNCTION__;
  t.label1 = label1;
  t.label2 = label2;

  {
    QMutexLocker locker(mAccessLock);
    mTodoList->append(t);
  }
}

void Graphs::ProcessRemoveNode(QString label) {
  if (NodeLookup->contains(label) == true) {
    mG->delNode(NodeLookup->value(label));
    NodeLookup->remove(label);
  }
}

void Graphs::ProcessRemoveLink(QString label1, QString label2) {
  QString nodeLookupLabel = NodeEdgeLabel(label1, label2);
  if (EdgeLookup->contains(nodeLookupLabel) == true) {

    mG->delEdge(EdgeLookup->value(nodeLookupLabel));
    EdgeLookup->remove(nodeLookupLabel);
  }
}

void Graphs::RemoveLink(QString label1, QString label2) {
  TTPLOG_FUNCTIONSCOPE;

  todo t;
  t.cmd = __FUNCTION__;
  t.label1 = label1;
  t.label2 = label2;

  {
    QMutexLocker locker(mAccessLock);
    mTodoList->append(t);
  }
}

void Graphs::ProcessSetCoord(QString label, qreal x, qreal y) {
  if (NodeLookup->contains(label) == true) {
    mGA->x(NodeLookup->value(label)) = x;
    mGA->y(NodeLookup->value(label)) = y;
  }
}

void Graphs::SetCoord(QString label, qreal x, qreal y) {
  TTPLOG_FUNCTIONSCOPE;

  todo t;
  t.cmd = __FUNCTION__;
  t.label1 = label;
  t.x1 = x;
  t.y1 = y;

  {
    QMutexLocker locker(mAccessLock);
    mTodoList->append(t);
  }
}

void Graphs::SetLinkValue(QString label1, QString label2, int value) {
  TTPLOG_FUNCTIONSCOPE;

  todo t;
  t.cmd = __FUNCTION__;
  t.label1 = label1;
  t.label2 = label2;
  t.x1 = value;
  t.y1 = 0;

  {
    QMutexLocker locker(mAccessLock);
    mTodoList->append(t);
  }
}

void Graphs::ProcessClearNodes(todo t) {
  Init();

  if (NodeLookup != NULL) {
    NodeLookup->clear();
  }

  if (EdgeLookup != NULL) {
    EdgeLookup->clear();
  }
}

void Graphs::ClearNodes() {
  TTPLOG_FUNCTIONSCOPE;
  todo t;
  t.cmd = __FUNCTION__;
  t.label1 = "";

  {
    QMutexLocker locker(mAccessLock);
    mTodoList->append(t);
  }
}

void Graphs::ProcessAddLink(QString label2, QString label1) {
  if (NodeLookup->contains(label1) == true &&
      NodeLookup->contains(label2) == true) {

    QString nodeLookupLabel = NodeEdgeLabel(label1, label2);

    edge e =
        mG->newEdge(NodeLookup->value(label1), NodeLookup->value(label2), 0);
    mGA->label(e) = nodeLookupLabel.toStdString();

    EdgeLookup->insert(nodeLookupLabel, e);
  }
}

void Graphs::ProcessSetLinkValue(QString label1, QString label2, int value) {
  if (NodeLookup->contains(label1) == true &&
      NodeLookup->contains(label2) == true) {
    QString nodeLookupLabel = NodeEdgeLabel(label1, label2);
    if (EdgeLookup->contains(nodeLookupLabel) == true) {
      edge e = EdgeLookup->value(nodeLookupLabel);
      mG->setWeight(e, value);
    }
  }
}

void Graphs::ProcessTodoList() {
  TTPLOG_FUNCTIONSCOPE;

  QString label, label1, label2;
  qreal x, y;

  QElapsedTimer mUpdateMax;

  mUpdateMax.start();

  bool isEmpty = false;

  mAccessLock->lock();
  isEmpty = mTodoList->isEmpty();
  TTPLOG_VALUE("mTodoList.size", mTodoList->size());
  mAccessLock->unlock();

  const int maxUpdateTimeMs = 1000.0 / 10.0;
  while (isEmpty == false && mUpdateMax.elapsed() < maxUpdateTimeMs) {

    mAccessLock->lock();
    todo t = mTodoList->takeFirst();
    isEmpty = mTodoList->isEmpty();
    mAccessLock->unlock();

    if (t.cmd == "Graphs::AddNewNode") {
      label = t.label1;
      ProcessAddNewNode(label);
    } else if (t.cmd == "Graphs::RemoveNode") {
      label = t.label1;
      ProcessRemoveNode(label);
    } else if (t.cmd == "Graphs::AddLink") {

      label1 = t.label1;
      label2 = t.label2;

      ProcessAddLink(label1, label2);
    } else if (t.cmd == "Graphs::SetLinkValue") {

      label1 = t.label1;
      label2 = t.label2;
      x = t.x1;

      ProcessSetLinkValue(label1, label2, x);
    } else if (t.cmd == "Graphs::RemoveLink") {
      label1 = t.label1;
      label2 = t.label2;
      ProcessRemoveLink(label1, label2);
    } else if (t.cmd == "Graphs::SetCoord") {
      label = t.label1;
      x = t.x1;
      y = t.y1;
      ProcessSetCoord(label, x, y);
    } else if (t.cmd == "Graphs::ClearNodes") {
      ProcessClearNodes(t);
    }
  }
}

void Graphs::layoutNodes() {
  TTPLOG_FUNCTIONSCOPE;

  if (mG->numberOfNodes() > 0) {
#if 1

    FMMMLayout fmmm;

    fmmm.useHighLevelOptions(true);
    fmmm.unitEdgeLength(75.0);
    fmmm.newInitialPlacement(false);
    // fmmm.qualityVersusSpeed(FMMMLayout::qvsGorgeousAndEfficient);
    fmmm.randSeed(1); // force deterministic behavior
    // FMMMLayout::AllowedPositions ap;
    // ap = FMMMLayout::apExponent;
    // fmmm.allowedPositions (ap);

    // fmmm.repulsiveForcesCalculation (FMMMLayout::rfcExact);

    fmmm.call(*mGA);
#elif 1
    SugiyamaLayout SL;
    SL.setRanking(new OptimalRanking);
    SL.setCrossMin(new MedianHeuristic);

    OptimalHierarchyLayout *ohl =
        new OptimalHierarchyLayout; // DoWe leak memory?
    ohl->layerDistance(10.0);
    ohl->nodeDistance(10.0);
    ohl->weightBalancing(0.8);
    SL.setLayout(ohl);
    SL.call(*mGA);

#elif 0
    PlanarizationLayout pl;

    SubgraphPlanarizer *crossMin = new SubgraphPlanarizer;

    FastPlanarSubgraph *ps = new FastPlanarSubgraph;
    ps->runs(100);
    VariableEmbeddingInserter *ves = new VariableEmbeddingInserter;
    ves->removeReinsert(rrAll);

    crossMin->setSubgraph(ps);
    crossMin->setInserter(ves);

    EmbedderMinDepthMaxFaceLayers *emb = new EmbedderMinDepthMaxFaceLayers;
    pl.setEmbedder(emb);

    OrthoLayout *ol = new OrthoLayout;
    ol->separation(50.0);
    ol->cOverhang(0.4);
    pl.setPlanarLayouter(ol);

    pl.call(*mGA);

#endif

    QHash<QString, NodeItem> *r = new QHash<QString, NodeItem>;

    *r = GetNodeCoords();
    emit nodeCoordCalculated(r);
  }
}

QHash<QString, NodeItem> Graphs::GetNodeCoords() {
  TTPLOG_FUNCTIONSCOPE;
  QHash<QString, NodeItem> ret;

  Graph G;
  node v;

  List<node> allNodes;
  mG->allNodes(allNodes);

  // forallnodes(v, *mG) {
  for (auto v : allNodes) {

    NodeItem nd;
    nd.label = QString(mGA->label(v).c_str());
    nd.x = mGA->x(v);
    nd.y = mGA->y(v);
    nd.diameter = mGA->weight(v);

    List<edge> edges;
    v->adjEdges(edges);

    for (auto edge : edges) {
      QString linkedLabel = QString(mGA->label(edge->target()).c_str());
      nd.linkedNodes.append(linkedLabel);
    }
    ret.insert(nd.label, nd);
  }

  return ret;
}

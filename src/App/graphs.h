#ifndef GRAPHS_H
#define GRAPHS_H

#include <QHash>
#include <QMutex>
#include <QObject>
#include <QString>
#include <QThread>
#include <QTimer>

#include "nodeitem.h"
#include "worker.h"

#include <ogdf/basic/Graph.h>
#include <ogdf/basic/Graph_d.h>
#include <ogdf/basic/graph_generators.h>
#include <ogdf/energybased/FMMMLayout.h>
#include <ogdf/fileformats/GraphIO.h>
#include <ogdf/layered/DfsAcyclicSubgraph.h>
#include <ogdf/layered/MedianHeuristic.h>
#include <ogdf/layered/OptimalHierarchyLayout.h>
#include <ogdf/layered/OptimalRanking.h>
#include <ogdf/layered/SugiyamaLayout.h>
#include <ogdf/planarity/PlanarizationLayout.h>
#include <ogdf/planarity/SubgraphPlanarizer.h>
#include <ogdf/planarity/VariableEmbeddingInserter.h>
//#include <ogdf/planarity/FastPlanarSubgraph.h>
#include <ogdf/layered/FastHierarchyLayout.h>
#include <ogdf/layered/OptimalHierarchyLayout.h>
#include <ogdf/orthogonal/OrthoLayout.h>
#include <ogdf/planarity/EmbedderMinDepthMaxFaceLayers.h>
//#include <ogdf/internal/steinertree/EdgeWeightedGraph.h>

using namespace ogdf;

class Graphs : public worker {
  Q_OBJECT
public:
  struct todo {
    QString cmd;
    QString label1;
    QString label2;
    qreal x1;
    qreal y1;
  };

  virtual void InitThis();

  Graphs();

  void AddNewNode(QString label);
  void RemoveNode(QString label);
  void AddLink(QString label1, QString label2);
  void RemoveLink(QString label1, QString label2);
  void SetCoord(QString label, qreal x, qreal y);
  void SetLinkValue(QString label1, QString label2, int value);
  void ClearNodes();

  QHash<QString, NodeItem> GetNodeCoords();
  QString NodeEdgeLabel(QString label1, QString label2);

  void ProcessAddNewNode(QString label);
  void ProcessRemoveNode(QString label);
  void ProcessRemoveLink(QString label1, QString label2);
  void ProcessSetCoord(QString label, qreal x, qreal y);
  void ProcessClearNodes(todo t);
  void ProcessAddLink(QString label2, QString label1);
  void ProcessSetLinkValue(QString label1, QString label2, int value);
signals:
  void nodeCoordCalculated(QHash<QString, NodeItem> *);
  void initReadySignal();
  void ProcessTodoListSignal();
public slots:
  void layoutNodes();
private slots:
  void ProcessTodoList();

private:
  void InitGraph();
  void Init();

  EdgeWeightedGraph<int> *mG;
  // randomSimpleGraph(G, 500, 500);
  GraphAttributes *mGA;
  QHash<QString, node> *NodeLookup;
  QHash<QString, edge> *EdgeLookup;

  QMutex *mAccessLock;
  QList<todo> *mTodoList;
  QTimer *mProcessTodoListTimer;
};

#endif // GRAPHS_H

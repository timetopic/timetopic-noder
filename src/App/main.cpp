/*

TimeToPic Noder visualizes networks real time. Tool for fast prototyping.

Copyright (c) 2015 Herwood Technologies Oy,
Erkki Salonen erkki@herwoodtechnologies.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "app.h"
#include "nodeview.h"
#include <QApplication>
#include <QQmlApplicationEngine>

int main(int argc, char *argv[]) {
  QApplication qapp(argc, argv);

  /* Enable high dpi scaling to windows dialogs */
  static const char ENV_VAR_QT_DEVICE_PIXEL_RATIO[] = "QT_DEVICE_PIXEL_RATIO";
  if (!qEnvironmentVariableIsSet(ENV_VAR_QT_DEVICE_PIXEL_RATIO) &&
      !qEnvironmentVariableIsSet("QT_AUTO_SCREEN_SCALE_FACTOR") &&
      !qEnvironmentVariableIsSet("QT_SCALE_FACTOR") &&
      !qEnvironmentVariableIsSet("QT_SCREEN_SCALE_FACTORS")) {
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
  }

  qmlRegisterType<NodeView>("com.herwoodtech.nodeview", 1, 0, "NodeViewer");
  qmlRegisterType<App>("com.herwoodtech.app", 1, 0, "AppControl");

  QQmlApplicationEngine engine;

  /* Load QML files from resources */
  engine.addImportPath("qrc:///");
  /* Load QT5 qml files from local directory */
  QString AppQmlPath =
      QCoreApplication::applicationDirPath() + "/Qt5QmlWindows";
  engine.addImportPath(AppQmlPath);

  QString AppPluginPath =
      QCoreApplication::applicationDirPath() + "/Qt5Plugins";
  engine.addPluginPath(AppPluginPath);

  engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

  return qapp.exec();
}

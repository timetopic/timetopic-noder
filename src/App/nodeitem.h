#ifndef NODEITEM_H
#define NODEITEM_H

#include <QList>
#include <QString>


class NodeItem
{
public:
    NodeItem();
    QString label;
    qreal x;
    qreal y;
    qreal diameter;
    QList<QString> linkedNodes;

};

#endif // NODEITEM_H

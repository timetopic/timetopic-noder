/*

TimeToPic Noder visualizes networks real time. Tool for fast prototyping.

Copyright (c) 2015 Herwood Technologies Oy,
Erkki Salonen erkki@herwoodtechnologies.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


#include <QThread>

#include "timetopiclogger.h"
#include "worker.h"

worker::worker() : mProcessingStarted(false)
{
    TTPLOG_FUNCTIONSCOPE;
}

worker::~worker()
{
    TTPLOG_FUNCTIONSCOPE;

}

bool worker::startProcessing(QThread *  /* sourceThread */ )
{
    TTPLOG_FUNCTIONSCOPE;
    /* Start thread. Operating system will call run method soon. */
    /* Move ownership of this object to new thread */
    this->moveToThread(this);
    start();
    return true;
}

void worker::stopProcessing()
{    
    QThread::exit(0);
}

void worker::SetConfiguration(QString configKey, QString configValue)
{
    TTPLOG_FUNCTIONSCOPE;
    /* This function should be called from subclass SetConfiguration too.
     * This just verifies that call sequence is correct */
    Q_UNUSED(configKey);
    Q_UNUSED(configValue);


    Q_ASSERT(mProcessingStarted==false);
}

void worker::startedSlot()
{
    TTPLOG_FUNCTIONSCOPE;

}

void worker::finishedSlot()
{
    TTPLOG_FUNCTIONSCOPE;
}


void worker::connectSignalsSlot()
{
    /* Before this, sub class run has been called */
    connect(this,SIGNAL(finished()),this,SLOT(finishedSlot()));
    connect(this,SIGNAL(started()),this,SLOT(startedSlot()));

    InitThis(); // Virtual call
}

void worker::run()
{
    TTPLOG_FUNCTIONSCOPE;


    connect(this,SIGNAL(InitObject()),this,SLOT(connectSignalsSlot()),Qt::QueuedConnection);

    emit InitObject();
    /* Now we started operation */
    mProcessingStarted = true;
    /* Everything happens now in event loop */
    exec();
    TTPLOG_STRING("exiting");
}

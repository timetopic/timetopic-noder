#ifndef WORKER_H
#define WORKER_H

#include <QThread>
#include <QObject>

class worker : public QThread
{
    Q_OBJECT
public:
    explicit worker();
    ~worker();

    /** @brief Start processing. This should called only once.  */
    virtual bool startProcessing(QThread *sourceThread);

    /** @brief Stop processing. This should called only once.  */
    virtual void stopProcessing();

    /** @brief Api for setting configuration. Call this before calling start processing */
    virtual void SetConfiguration(QString configKey, QString configValue);

    /** @brief This is called once thread object has been moved. Put member initialization here
        such as timers etc that requires event loop (member QObjects) */
    virtual void InitThis() = 0;
signals:
    void InitObject(); // this is signaled from run function and will result calling connectSignalsSlot in thread event loop
public slots:
    virtual void startedSlot();
    virtual void finishedSlot();    
private slots:
    void connectSignalsSlot();
protected:
    virtual void run();
private:
    bool mProcessingStarted;
};

#endif // WORKER_H

#include "colorgenerator.h"

ColorGenerator::ColorGenerator()
{
    mGradientColors.append(QColor(Qt::blue));
    mGradientColors.append(QColor(Qt::cyan));
    mGradientColors.append(QColor(Qt::green));
    mGradientColors.append(QColor(Qt::yellow));
    mGradientColors.append(QColor(Qt::red));

}
// http://stackoverflow.com/questions/3306786/get-intermediate-color-from-a-gradient
QColor ColorGenerator::getColor(double value)
{
    QColor color(Qt::black);

    if (value>=0 && value<=1.0) {

    //Assume mGradientColors.count()>1 and value=[0,1]
    double stepbase = 1.0/(mGradientColors.count()-1);
    int interval=mGradientColors.count()-1; //to fix 1<=0.99999999;

    for (int i=1; i<mGradientColors.count();i++)//remove begin and end
    {
        if(value<=i*stepbase ){interval=i;break;}
    }
    double percentage = (value-stepbase*(interval-1))/stepbase;
    color = QColor (interpolate(mGradientColors[interval],mGradientColors[interval-1],percentage));
    }
    else {
        // outside limits
    }
    return color;
}

QColor ColorGenerator::interpolate(QColor start, QColor end, double ratio)
{
    int r = (int)(ratio*start.red() + (1-ratio)*end.red());
    int g = (int)(ratio*start.green() + (1-ratio)*end.green());
    int b = (int)(ratio*start.blue() + (1-ratio)*end.blue());
    return QColor::fromRgb(r,g,b);
}


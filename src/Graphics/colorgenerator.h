#ifndef COLORGENERATOR_H
#define COLORGENERATOR_H

#include <QColor>
#include <QList>

class ColorGenerator
{
public:
    ColorGenerator();
    QColor getColor(double value);

private:
    QColor interpolate(QColor start,QColor end,double ratio);

    QList<QColor> mGradientColors;

};

#endif // COLORGENERATOR_H

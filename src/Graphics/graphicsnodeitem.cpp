/*

TimeToPic Noder visualizes networks real time. Tool for fast prototyping.

Copyright (c) 2015 Herwood Technologies Oy,
Erkki Salonen erkki@herwoodtechnologies.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <QDebug>
#include <QGraphicsScene>
#include <QGraphicsView>

#include "graphicsnodeitem.h"

const int edgeLen = 10;

GraphicsNodeItem::GraphicsNodeItem()
{
    this->setFlag(QGraphicsItem::ItemIgnoresTransformations,true);
}

void GraphicsNodeItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

    QRectF mybox = QRect(0,0,mDiameter*edgeLen,mDiameter*edgeLen);
    mybox.moveCenter(QPointF(0,0));

    QColor myColor;
    if (mNamedColorName.length()>0) {
        myColor = QColor(mNamedColorName);
    }
    else {
        myColor = QColor(Qt::red);
    }

    QPen myPen;
    myPen.setWidth(0);
    myPen.setColor(myColor);
    painter->setPen(myPen);
    painter->setBrush(myColor);
    painter->drawEllipse(mybox);

    if ( this->scene()->views().first()->transform().m11()>1.0 ) {
        if (mLabel.length()>0) {            
            QFont serifFont;
            QFontMetricsF m(serifFont);
            serifFont.setPixelSize(12);
            serifFont.setBold(true);

            painter->setFont(serifFont);
            QPointF p(mybox.center());

            p.setX(mybox.center().x()-m.tightBoundingRect(mLabel).width()/2.0);
            p.setY(mybox.top()-mybox.height());
            painter->drawText(p,mLabel);
        }
    }
}
QString GraphicsNodeItem::namedColorName() const
{
    return mNamedColorName;
}

void GraphicsNodeItem::setNamedColorName(const QString &namedColorName)
{
    mNamedColorName = namedColorName;
}
qreal GraphicsNodeItem::diameter() const
{
    return mDiameter;
}

void GraphicsNodeItem::setDiameter(const qreal &diameter)
{
    mDiameter = diameter;
}
QString GraphicsNodeItem::label() const
{
    return mLabel;
}

void GraphicsNodeItem::setLabel(const QString &label)
{
    mLabel = label;
}
QString GraphicsNodeItem::nodeName() const
{
    return mNodeName;
}

void GraphicsNodeItem::setNodeName(const QString &nodeName)
{
    mNodeName = nodeName;
}





QRectF GraphicsNodeItem::boundingRect() const
{
    QRectF ret = QRect(0,0,mDiameter*edgeLen,mDiameter*edgeLen);
    ret.moveCenter(QPointF(0,0));

    return ret;
}


#ifndef GRAHPICSNODEITEM_H
#define GRAHPICSNODEITEM_H

#include <QGraphicsItem>
#include <QRectF>
#include <QPainter>
#include <QString>
class GraphicsNodeItem : public QGraphicsItem
{
public:
    GraphicsNodeItem();

    QRectF boundingRect() const;

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    QString namedColorName() const;
    void setNamedColorName(const QString &namedColorName);

    qreal diameter() const;
    void setDiameter(const qreal &diameter);

    QString label() const;
    void setLabel(const QString &label);

    QString nodeName() const;
    void setNodeName(const QString &nodeName);

private:
    QString mNamedColorName;
    qreal   mDiameter;
    QString mLabel;
    QString mNodeName;
};

#endif // GRAHPICSNODEITEM_H

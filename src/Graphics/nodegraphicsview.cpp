/*

TimeToPic Noder visualizes networks real time. Tool for fast prototyping.

Copyright (c) 2015 Herwood Technologies Oy,
Erkki Salonen erkki@herwoodtechnologies.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "nodegraphicsview.h"
#include "graphicsnodeitem.h"
#include "timetopiclogger.h"

#include <QDir>
#include <QFileInfo>
#include <QLine>
#include <QPainter>
#include <QResizeEvent>

NodeGraphicsView::NodeGraphicsView(QWidget *parent)
    : QGraphicsView(parent), mViewCopy(nullptr), mNodes(nullptr),
      mBackGroundImage(nullptr), mLinks(nullptr) {

  this->setRenderHints(QPainter::Antialiasing |
                       QPainter::SmoothPixmapTransform |
                       QPainter::TextAntialiasing);
  this->setViewportUpdateMode(QGraphicsView::NoViewportUpdate);
}

void NodeGraphicsView::RenderToCacheImage() {
  TTPLOG_FUNCTIONSCOPE;
  QRect clipRect;
  clipRect = this->viewport()->rect();

  if (mViewCopy != nullptr) {
    if (mViewCopy->rect() != clipRect) {
      delete mViewCopy;
      mViewCopy = nullptr;
    }
  }

  if (mViewCopy == nullptr) {

    mViewCopy =
        new QImage(clipRect.width(), clipRect.height(), QImage::Format_ARGB32);
  }
  QPainter m(mViewCopy);
  m.setBrush(Qt::lightGray);
  m.drawRect(mViewCopy->rect());

  this->render(&m, clipRect, clipRect, Qt::IgnoreAspectRatio);
#if 0
    m.setPen(Qt::blue);
    m.setBrush(Qt::transparent);
    m.drawEllipse(0,0,mViewCopy->width(),mViewCopy->height());
#endif

  emit qGraphicsViewPaintRegSignal();
}

void NodeGraphicsView::SetCurrentNodeList(QHash<QString, GraphNode *> *nodes) {
  mNodes = nodes;
}

void NodeGraphicsView::SetCurrentLinkList(QHash<QString, GraphLink *> *links) {
  mLinks = links;
}

QRectF NodeGraphicsView::LoadBackGroundImage(QString fileNameWithPath) {
  QRectF ret;
  if (fileNameWithPath.length() > 0) {
    QFileInfo myFile(fileNameWithPath);

    if (myFile.exists() == true) {
      if (mBackGroundImage != nullptr) {
        delete mBackGroundImage;
        mBackGroundImage = nullptr;
      }
      mBackGroundImage = new QImage(fileNameWithPath);
      ret = mBackGroundImage->rect();
    } else {
      if (mBackGroundImage != nullptr) {
        delete mBackGroundImage;
        mBackGroundImage = nullptr;
      }
      ret = QRectF();
    }
  } else {
    if (mBackGroundImage != nullptr) {
      delete mBackGroundImage;
      mBackGroundImage = nullptr;
    }
    ret = QRectF();
  }
  return ret;
}

void NodeGraphicsView::paintEvent(QPaintEvent *event) {
  TTPLOG_FUNCTIONSCOPE;
  QGraphicsView::paintEvent(event);
  mPrevPaintTime = QDateTime::currentDateTime();
}

void NodeGraphicsView::DrawGrid(QPainter *painter, const QRectF &rect) {
  const int edge = 100;
  int addCounter = 0;

  QColor Color1(QStringLiteral("#d8d8d8"));
  QColor Color2(QStringLiteral("#e4e4e4"));
  QColor Color3(QStringLiteral("#968e8e"));
  QColor ColorFont(Qt::lightGray);
  const qreal minTextScaleThreshold = 1.0;

  QFont serifFont("Times", 10, QFont::Bold);
  serifFont.setPointSize(12);
  QFontMetrics m(serifFont);

  painter->setFont(serifFont);
  this->setRenderHints(QPainter::TextAntialiasing);

  QPointF p1, p2;
  p1 = mapToScene(this->viewport()->rect().topLeft());
  p2 = mapToScene(this->viewport()->rect().bottomRight());
  QRectF viewportInSceneCoord(p1 * (1.0 / transform().m11()),
                              p2 * (1.0 / transform().m22()));

  QRectF box(0, 0, edge, edge);
  int xDiv = p1.x() / edge;
  int yDiv = p1.y() / edge;

  QPointF offset(xDiv * edge, yDiv * edge);

  bool drawTexts;
  if (this->transform().m11() > minTextScaleThreshold) {
    drawTexts = true;
  } else {
    drawTexts = false;
  }

  if (drawTexts == true) {

    // *2 draw a bit beyound to ensure that no gaps when zooming in and out

    for (int y = offset.y(); y < offset.y() + viewportInSceneCoord.height() * 2;
         y += edge) {

      addCounter = 0;

      addCounter += (y / edge) % 2;

      for (int x = offset.x();
           x < offset.x() + viewportInSceneCoord.width() * 2; x += edge) {

        if (addCounter % 2 == 0) {
          painter->setPen(Color1);
          painter->setBrush(Color1);
        } else {
          painter->setPen(Color2);
          painter->setBrush(Color2);
        }

        box.moveTopLeft(QPointF(x, y));

        painter->drawRect(box);

        addCounter++;
      }
    }

    for (int y = offset.y(); y < offset.y() + viewportInSceneCoord.height() * 2;
         y += edge) {

      addCounter = 0;

      for (int x = offset.x();
           x < offset.x() + viewportInSceneCoord.width() * 2; x += edge) {

        box.moveTopLeft(QPointF(x, y));
        const qreal textMargin = 3.0;

        painter->setPen(ColorFont);
        painter->setBrush(ColorFont);
        QPoint target = mapFromScene(box.topLeft());
        target = QPoint((qreal)target.x() + (qreal)textMargin,
                        target.y() + m.height());

        painter->setWorldMatrixEnabled(false);

        painter->setPen(Color3);
        painter->setBrush(Color3);

        if (x == offset.x()) {
          const int xLeft = 20;
          target.setX(xLeft);

          if (target.y() >= xLeft * 2) {
            painter->drawText(target, QString("%1").arg(y));
          }
        }
        if (y == offset.y()) {
          const int yTop = 20;
          target.setY(yTop);

          if (target.x() >= yTop * 2) {
            painter->drawText(target, QString("%1").arg(x));
          }
        }

        painter->setWorldMatrixEnabled(true);

        addCounter++;
      }
    }

  } else {
    painter->setPen(Color2);
    painter->setBrush(Color2);
    painter->drawRect(rect);
  }
}

void NodeGraphicsView::drawBackroundImage(QPainter *painter,
                                          const QRectF &rect) {
  TTPLOG_FUNCTIONSCOPE;

  Q_UNUSED(rect);

  if (mBackGroundImage != NULL) {
    painter->drawImage(mBackGroundImage->rect(), *mBackGroundImage,
                       mBackGroundImage->rect());
  } else {
    DrawGrid(painter, rect);
  }
}

void NodeGraphicsView::drawBackground(QPainter *painter, const QRectF &rect) {
  TTPLOG_FUNCTIONSCOPE;

  painter->setClipping(false);

  drawBackroundImage(painter, rect);
  drawLinks(painter, rect);

#if 0
    painter->drawLine(this->sceneRect().topLeft(),this->sceneRect().bottomRight());
    painter->drawLine(mViewCopy->rect().topRight(),mViewCopy->rect().bottomLeft());
#endif
}

void NodeGraphicsView::drawLinks(QPainter *painter, const QRectF &rect) {
  TTPLOG_FUNCTIONSCOPE;
  const int defaultPenWidth = 0;
  if (mNodes != NULL && mLinks != NULL) {

    painter->setClipRect(this->sceneRect());
    painter->setRenderHints(QPainter::Antialiasing, true);

    foreach (GraphNode *n, *mNodes) {

      foreach (QString link, n->linkedNodes.keys()) {

        QPointF p1(n->CurrentCoordX, n->CurrentCoordY);
        GraphNode *n2 = mNodes->value(link);

        if (n2 != NULL) {
          QPointF p2(n2->CurrentCoordX, n2->CurrentCoordY);

          QLineF l1(p1, p2);

          QRectF boundingBox(p1, p2);
          /* Make sure that intersects produces result */
          boundingBox.setWidth(boundingBox.width() + 1);
          boundingBox.setWidth(boundingBox.height() + 1);

          if (rect.intersects(boundingBox) == true) {

            QString linkName =
                QString("%1%2").arg(n->mNodeName).arg(n2->mNodeName);

            QPen myPen;

            if (mLinks->contains(linkName) == true) {
              GraphLink *link = mLinks->value(linkName);

              myPen.setColor(QColor(link->linkVisualizationColor()));
              myPen.setWidth(link->linkValue());

            } else {
              const QString defaultColor = "black";
              myPen.setColor(QColor(defaultColor));
              myPen.setWidth(defaultPenWidth);
            }

            myPen.setCosmetic(true);
            painter->setPen(myPen);

            painter->drawLine(l1);
          }
        }
      }
    }
  }
}

void NodeGraphicsView::drawForeground(QPainter *painter, const QRectF &rect) {
  Q_UNUSED(painter);
  Q_UNUSED(rect);
  TTPLOG_FUNCTIONSCOPE;
}
QImage *NodeGraphicsView::viewCopy() const { return mViewCopy; }

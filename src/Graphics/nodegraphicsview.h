#ifndef NODEGRAPHICSVIEW_H
#define NODEGRAPHICSVIEW_H

#include "nodeitem.h"
#include "graphicsnodeitem.h"
#include "graphnode.h"
#include "graphlink.h"
#include <QHash>
#include <QGraphicsView>
#include <QDateTime>

class NodeGraphicsView : public QGraphicsView
{
Q_OBJECT

public:
    NodeGraphicsView(QWidget * parent = 0);

    void RenderToCacheImage();
    void SetCurrentNodeList(QHash<QString,GraphNode *> * nodes);
    void SetCurrentLinkList(QHash<QString,GraphLink *> * links);
    QRectF LoadBackGroundImage(QString fileNameWithPath);

    void SetBackPicturedrawingMode(bool enabled);

    void paintEvent(QPaintEvent * event);


    void drawBackground(QPainter *painter, const QRectF &rect);
    void drawForeground(QPainter *painter, const QRectF &rect);
    QImage *viewCopy() const;
    void drawLinks(QPainter *painter, const QRectF &rect);
    void drawBackroundImage(QPainter *painter, const QRectF &rect);
    void DrawGrid(QPainter *painter, const QRectF &rect);
signals:
    void qGraphicsViewPaintRegSignal();

private:
    QImage * mViewCopy;
    QImage * mBackGroundImage;    
    QHash<QString,GraphNode *> * mNodes;
    QHash<QString,GraphLink *> * mLinks;
    QDateTime mPrevPaintTime;

};

#endif // NODEGRAPHICSVIEW_H

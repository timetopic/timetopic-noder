/*

TimeToPic Noder visualizes networks real time. Tool for fast prototyping.

Copyright (c) 2015 Herwood Technologies Oy,
Erkki Salonen erkki@herwoodtechnologies.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "nodeview.h"
#include "nodegraphicsview.h"

#include "app.h"

NodeView::NodeView(QQuickPaintedItem *parent)
    : QQuickPaintedItem(parent), mView(NULL) {
  setRenderTarget(QQuickPaintedItem::Image);

  connect(this, SIGNAL(contentsScaleChanged()), this,
          SLOT(contentsScaleChangedSlot()));
  connect(this, SIGNAL(contentsSizeChanged()), this,
          SLOT(contentsSizeChangedSlot()));

  connect(App::Instance(), SIGNAL(sceneSizeChangeReqSignal(QRectF)), this,
          SLOT(sceneChangeReqSlot(QRectF)));

  connect(&mMouseOverTimer, SIGNAL(timeout()), this,
          SLOT(mouseOverTimeOutCheckSLot()));

  setAcceptedMouseButtons(Qt::AllButtons);
  setAcceptHoverEvents(true);
}

void NodeView::paint(QPainter *painter) {
  TTPLOG_FUNCTIONSCOPE;
  TTPLOG_VALUE("NodeView.paintInterval",
               mPrevPaintTime.msecsTo(QDateTime::currentDateTime()));
  if (mView != NULL) {

    if (mView->viewCopy() != NULL) {
      painter->drawImage(QPoint(0, 0), *mView->viewCopy());
    }
  }

  mPrevPaintTime = QDateTime::currentDateTime();
}

void NodeView::SetGraphicsView() {
  mView = App::Instance()->view();
  connect(mView, SIGNAL(qGraphicsViewPaintRegSignal()), this,
          SLOT(PaintRequestSlotFromNodeGraphicsViewSlot()));

  mView->setAlignment(Qt::AlignLeft | Qt::AlignTop);
}

void NodeView::mouseWheel(qreal angle) {
  QRect myBox = this->mView->viewport()->rect();

  qreal inc = 1.20;
  qreal scaleVal = 1.0;

  if (angle > 0) {
    myBox.setWidth(myBox.width() * inc);
    myBox.setHeight(myBox.height() * inc);
    scaleVal = inc;
  } else {
    myBox.setWidth(myBox.width() * 1.0 / inc);
    myBox.setHeight(myBox.height() * 1.0 / inc);
    scaleVal = 1.0 / inc;
  }
  qreal maxScale = 2.0;
  qreal minScale = 0.05;

  if (mView->transform().m11() * scaleVal > minScale &&
      mView->transform().m11() * scaleVal < maxScale) {

    myBox.moveCenter(this->mView->viewport()->rect().center());
    mView->scale(scaleVal, scaleVal);
  }
}

void NodeView::mouseLeftButtonPress(qreal x, qreal y) {
  mPanStartScene = mView->mapToScene(QPoint(x, y));
}

void NodeView::mouseRightButtonPress(qreal x, qreal y) {
  Q_UNUSED(x);
  Q_UNUSED(y);
}

void NodeView::mouseLeftButtonRelease(qreal x, qreal y) {
  Q_UNUSED(x);
  Q_UNUSED(y);
  mPanStartScene = QPointF();
}

void NodeView::mouseRightButtonRelease(qreal x, qreal y) {
  Q_UNUSED(x);
  Q_UNUSED(y);
}

void NodeView::mouseMove(qreal x, qreal y) {
  if (mPanStartScene.isNull() == false) {

    QPointF mappedCurrentCoord = mView->mapToScene(QPoint(x, y));

    int dx = mPanStartScene.x() - mappedCurrentCoord.x();
    int dy = mPanStartScene.y() - mappedCurrentCoord.y();

    QRect myBox = this->mView->viewport()->rect();
    QPointF p1, p2;
    p1 = mView->mapToScene(myBox.topLeft());
    p2 = mView->mapToScene(myBox.bottomRight());

    QRectF mappedRectF = QRectF(p1, p2);

    mappedRectF.moveCenter(
        QPointF(mappedRectF.center().x() + dx, mappedRectF.center().y() + dy));

    mView->centerOn(mappedRectF.center());
  } else {
    const int mouseOverTimeOutMSec = 500;
    mMouseOverTimer.start(mouseOverTimeOutMSec);
    mMouseOverPointAsSceneCoords = mView->mapToScene(QPoint(x, y));
  }
}

void NodeView::init() { SetGraphicsView(); }

void NodeView::contentsScaleChangedSlot() {}

void NodeView::contentsSizeChangedSlot() {}

void NodeView::sceneChangeReqSlot(QRectF r) {
  TTPLOG_FUNCTIONSCOPE;

  if (r.width() == 0 && r.height() == 0) {
    r = QRect(0, 0, width(), height());
  }

  UpdateQGraphicsViewSize(r);
}

void NodeView::PaintRequestSlotFromNodeGraphicsViewSlot() {
  TTPLOG_FUNCTIONSCOPE;
  this->update(QRect(0, 0, this->width(), this->height()));
}

void NodeView::mouseOverTimeOutCheckSLot() {
  mMouseOverTimer.stop();

  if (mView != NULL) {
    GraphicsNodeItem *p;
    QGraphicsItem *pCand = 0;

    const int defaultLookUpEdgeInScene = 50;
    QRectF lookup(0, 0, defaultLookUpEdgeInScene, defaultLookUpEdgeInScene);
    lookup.moveCenter(mMouseOverPointAsSceneCoords);

    QList<QGraphicsItem *> myItems = mView->scene()->items(lookup);

    if (myItems.size() > 0) {
      pCand = (QGraphicsItem *)myItems.first();
      foreach (QGraphicsItem *myitem, myItems) {
        QLineF distanceVectA(mMouseOverPointAsSceneCoords, myitem->pos());
        QLineF distanceVectB(pCand->pos(), myitem->pos());

        if (distanceVectA.length() < distanceVectB.length()) {
          pCand = myitem;
        }
      }
    }

    p = (GraphicsNodeItem *)pCand;
    if (p != NULL) {
      QString text = QString("%1;%2;%3;%4")
                         .arg(p->nodeName())
                         .arg((int)p->pos().x())
                         .arg((int)p->pos().y())
                         .arg(p->label());
      emit mouseOverTextAvailable(text);
    }
  }
}

void NodeView::UpdateQGraphicsViewSize(QRectF r) {
  TTPLOG_FUNCTIONSCOPE;
  if (mView != nullptr) {

    mView->RenderToCacheImage();
    mView->ensureVisible(r);
    mView->fitInView(r, Qt::KeepAspectRatio);
  }
}

void NodeView::geometryChanged(const QRectF &newGeometry,
                               const QRectF &oldGeometry) {
  TTPLOG_FUNCTIONSCOPE;

  QRectF v = newGeometry;
  if (mView != nullptr) {

    mView->setFixedWidth(v.width());
    mView->setFixedHeight(v.height());

    UpdateQGraphicsViewSize(v);
  }

  QQuickPaintedItem::geometryChanged(newGeometry, oldGeometry);
  if (mView != nullptr) {
    mView->setViewport(nullptr);
  }
}

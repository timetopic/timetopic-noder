#ifndef NODEVIEW_H
#define NODEVIEW_H

#include <QQuickPaintedItem>
#include "nodegraphicsview.h"
#include <QGraphicsScene>
#include <QTimer>
#include <QTimer>

class NodeView : public QQuickPaintedItem
{
    Q_OBJECT
public:
    NodeView(QQuickPaintedItem *parent = 0);

    void paint(QPainter * painter);
    void SetGraphicsView();

    Q_INVOKABLE void mouseWheel(qreal angle);
    Q_INVOKABLE void mouseLeftButtonPress(qreal x, qreal y);
    Q_INVOKABLE void mouseRightButtonPress(qreal x, qreal y);
    Q_INVOKABLE void mouseLeftButtonRelease(qreal x, qreal y);
    Q_INVOKABLE void mouseRightButtonRelease(qreal x, qreal y);

    Q_INVOKABLE void mouseMove(qreal x,qreal y);

    Q_INVOKABLE void init();
    void UpdateQGraphicsViewSize(QRectF r);
signals:
    void delayedPaintSignal();

    void mouseOverTextAvailable(QString mouseOver);
public slots:
    void contentsScaleChangedSlot();
    void contentsSizeChangedSlot();
    void sceneChangeReqSlot(QRectF r);
    void PaintRequestSlotFromNodeGraphicsViewSlot();
private slots:
    void mouseOverTimeOutCheckSLot();
protected:


    void geometryChanged(const QRectF & newGeometry, const QRectF & oldGeometry);

private:
    NodeGraphicsView * mView;        
    QDateTime mPrevPaintTime;
    QPointF mPanStartScene;
    QTimer mMouseOverTimer;
    QPointF mMouseOverPointAsSceneCoords;
};

#endif // NODEVIEW_H

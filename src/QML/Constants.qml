import QtQuick 2.0

Item {

    readonly property color blueColor: "#676cff"
    readonly property color tooltipBgColor: "#f0f0f0"

    readonly property int bigFontSize: 18
    readonly property int smalltextFontSize: 14
}


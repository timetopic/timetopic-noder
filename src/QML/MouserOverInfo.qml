import QtQuick 2.0

Rectangle {
    id: myMouseOverInfo

    property int nodeX: 123
    property int nodeY: 432
    property string nodename: "MyNode"
    property string nodelabel: "My node label testing text"


    color: consts.tooltipBgColor
    border.color: "gray"
    width: 250
    height: freetextlabel.y + freetextlabel.height + (nodelabel != "" ? defaultMargin : 0)


    // NODENAME
    Text {
        id: nodenameText
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: defaultMargin
        clip: true
        text: nodename
        font.pixelSize: consts.bigFontSize
    }

    // COORDINATES
    Row {
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.margins: defaultMargin
        Text {
            text: "x"
            color: consts.blueColor
            font.pixelSize: consts.smalltextFontSize
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 1
        }
        Text {
            text: nodeX
            width: myMouseOverInfo.width*0.15
            font.pixelSize: consts.bigFontSize
        }
        Item { // Spacer
            width: 5
            height: 1
        }

        Text {
            text: "y"
            color: consts.blueColor
            font.pixelSize: consts.smalltextFontSize
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 1
        }
        Text {
            text: nodeY
            width: myMouseOverInfo.width*0.15
            font.pixelSize: consts.bigFontSize
        }
    }


    // FREETEXT LABEL
    Text {
        id: freetextlabel
        anchors.top: nodenameText.bottom
        anchors.left: parent.left
        anchors.margins: defaultMargin
        anchors.right: parent.right
        font.pixelSize: consts.smalltextFontSize
        clip: true
        text: nodelabel
        wrapMode: Text.WordWrap
        color: "gray"
    }
}

/*

TimeToPic Noder visualizes networks real time. Tool for fast prototyping.

Copyright (c) 2015 Herwood Technologies Oy,
Erkki Salonen erkki@herwoodtechnologies.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.0
import QtQuick 2.0
import QtQuick.Window 2.1

import com.herwoodtech.nodeview 1.0
import com.herwoodtech.app 1.0

ApplicationWindow {
    visible: true
    width: 1000
    height: 1000
    title: qsTr("TimeToPic Noder")

    property int defaultMargin: 6

    Component.onCompleted: {
        myNodeView.init();
        mySplash.setPicture("qrc:///assets-splash.png",true)
        myToolbar.setLayOutMode(true)
    }

    Constants {
        id: consts
    }

    AppControl {
        id:myAppControl

        property string dateString
        property var locale: Qt.locale()


        onAnimationReqSignal: {
            update()
        }

        onProcessedmessagescountChanged: {
            processedMessagesStatus.text = "Processed msgs " + processedmessagescount
            var myDate = new Date()
            dateString = myDate.toLocaleTimeString( locale,
                                                "hh:mm:ss")
            myLatestMessageTimeStatus.text = "Last msg time " + dateString
        }

        onNodecountChanged: {
            nodeCountStatus.text = "Nodes count "+ nodecount
        }

        onParseproblemscounterChanged: {
            myStatusErrCounterStatus.text = "Parse errors count "+ parseproblemscounter
        }
        onConnectionstatusChanged: {
            myConnStatus.text = connectionstatus
        }
    }

    statusBar: StatusBar {
        RowLayout {
            anchors.fill: parent
            Label {
                id: nodeCountStatus
                text: "Nodes count 0"
            }
            Label {
                id: processedMessagesStatus
                text: "Processed msgs 0"
            }
            Label {
                id: myStatusErrCounterStatus
                text: "Parse errors count 0"
            }
            Label {
                id: myConnStatus
                text: "" }
            Label {
                id: myLatestMessageTimeStatus
                text: "" }
        }
    }

    toolBar:ToolBar {
        height: 40
            id: myToolbar
            function setLayOutMode(disableCoords) {
                if (disableCoords===true) {
                    modeButton.iconSource= "qrc:///Buttons-toLayoutMode.png"
                    myAppControl.setDisableCoordinates(true)
                }
                else {
                    modeButton.iconSource= "qrc:///Buttons-toCoordMode.png"
                    myAppControl.setDisableCoordinates(false)
                }
                modeButton.mode=!disableCoords
            }

           RowLayout {
               anchors.fill: parent
               spacing: 0

               ToolButton {
                   id: modeButton
                   property bool mode: true
                   iconSource: "qrc:///Buttons-toCoordMode.png"
                   onClicked: {
                        myToolbar.setLayOutMode(mode)
                   }
/*
                   Rectangle {
                       z: -1
                       width: parent.width
                       height: parent.height
                       color: "green"
                       anchors.centerIn: parent
                       visible: parent.mode
                       opacity: 0.5

                   }*/

               }

               Item { // Spacer
                   width: defaultMargin * 2
                   height: 1
               }


               ToolButton {
                   iconSource: "qrc:///Buttons-demo1-autolayout.png"
                   onClicked: {
                       myAppControl.stopTestCases()
                       myAppControl.startTestCase(1) // testdatagen.cpp testcases enum
                       demoStop.visible=true
                   }
               }

               ToolButton {
                   iconSource: "qrc:///Buttons-demo2-coordinates.png"
                   onClicked: {                   
                       myToolbar.setLayOutMode(false)
                       myAppControl.stopTestCases()
                       myAppControl.startTestCase(2)// testdatagen.cpp testcases enum
                       demoStop.visible=true
                   }
               }

               ToolButton {
                   iconSource: "qrc:///Buttons-demo3-coloring.png"
                   onClicked: {                   
                       /* This use case requires autolayout mode */
                       myToolbar.setLayOutMode(true)

                       myAppControl.stopTestCases()
                       myAppControl.startTestCase(4)// testdatagen.cpp testcases enum
                       demoStop.visible=true
                   }
               }

               ToolButton {
                   iconSource: "qrc:///Buttons-demo-stop.png"
                   id:demoStop
                   visible: false
                   onClicked: {
                       /* This use case requires autolayout mode */
                       myAppControl.stopTestCases()
                       demoStop.visible=false
                   }
               }
               Item { // Spacer
                   width: defaultMargin * 2
                   height: 1
               }

               ToolButton {
                   iconSource: "qrc:///Buttons-helpButton.png"
                   onClicked: {
                       mySplash.setPicture("assets-help.png",false)
                   }
               }
               ToolButton {
                   iconSource: "qrc:///Buttons-aboutButton.png"
                   onClicked: {
                       mySplash.setPicture("assets-about.png",false)
                   }
               }

               Item { Layout.fillWidth: true }

           }
       }

    NodeViewer {
        id:myNodeView
        clip: true
        anchors.fill: parent

        property int defaultMouseOverOffsetX: 30
        property int defaultMouseOverOffsetY: 10

        onMouseOverTextAvailable: {

            if (mouseOver!=="") {
                myMouseOverInfo.x = myMouse.mouseX+defaultMouseOverOffsetX
                myMouseOverInfo.y = myMouse.mouseY+defaultMouseOverOffsetY
                myMouseOverInfo.setText(mouseOver)

                myMouseOverInfo.visible=true
            }
            else {
                myMouseOverInfo.visible=false
            }
        }

        MouseArea {
            id: myMouse
            enabled: true
            acceptedButtons: Qt.LeftButton | Qt.RightButton
            anchors.fill: parent
            hoverEnabled: true

            onWheel: {
                  myNodeView.mouseWheel(wheel.angleDelta.y);
            }
            onPressed: {
                if (mouse.button == Qt.LeftButton) {
                    myNodeView.mouseLeftButtonPress(mouse.x,mouse.y)
                }
                else if ( mouse.button == Qt.RightButton) {
                    myNodeView.mouseRightButtonPress(mouse.x,mouse.y)
                }
            }
            onReleased: {
                if (mouse.button == Qt.LeftButton) {
                    myNodeView.mouseLeftButtonRelease(mouse.x,mouse.y)
                }
                else if ( mouse.button == Qt.RightButton) {
                    myNodeView.mouseRightButtonRelease(mouse.x,mouse.y)
                }
            }

            onMouseXChanged: {
                myNodeView.mouseMove(mouseX,mouseY)
                myMouseOverInfo.visible=false
            }
            onMouseYChanged: {
                myNodeView.mouseMove(mouseX,mouseY)
                myMouseOverInfo.visible=false
            }
        }
    }

    MouserOverInfo {
        id: myMouseOverInfo

        function setText(myText) {
            var list = myText.split(";")
            if(list.length === 4)
            {
                nodename = list[0]
                nodeX = list[1]
                nodeY = list[2]
                nodelabel = list[3]
            }
            else
            {

                nodename = "--"
                nodeX = "--"
                nodeY = "--"
                nodelabel = "--"
            }
        }
    }

    Rectangle {
        anchors.fill: parent
        color: "black"
        opacity: 0.4
        visible: mySplash.visible
    }

    Rectangle {
        id: mySplash

        function setPicture(pictureName, enableTimeOut) {
            /* fore example 'qrc:///assets-splash.png' */
            splashImage.source=pictureName
            mySplash.visible=true
            if (enableTimeOut===true) {
                splashTimeOut.running=true;
            }
        }

        anchors.centerIn: parent
        visible: false
        width: splashImage.width
        height: splashImage.height

        Image {
            id: splashImage
            source: ""
            MouseArea {
                anchors.fill: parent
                onClicked:  {
                    mySplash.visible = false
                }
            }
        }

        Timer {
            id: splashTimeOut
            interval: 5000; running: true; repeat: false
            onTriggered: {
                mySplash.visible = false
            }
        }
    }
}

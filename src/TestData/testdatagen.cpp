/*

TimeToPic Noder visualizes networks real time. Tool for fast prototyping.

Copyright (c) 2015 Herwood Technologies Oy,
Erkki Salonen erkki@herwoodtechnologies.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <QDateTime>

#include "testdatagen.h"

// Amount of nodes for tests
const int DefaultTestItemsCount = 20;
const int defaultEdge = 100;
const int defaultColumnCount = 10;
// testcase cycle time
const int singletestTimeMsec = 60000;

TestDataGen::TestDataGen() {}

void TestDataGen::InitThis() {

  mGeneratorTimer = new QTimer;
  mTestRecalcTimer = new QElapsedTimer;
  mTestCaseChangeTimer = new QElapsedTimer;
  mLinks = new QHash<QString, NodeLink>;
  mNodes = new QHash<QString, QString>;

  mTestCaseNumber = TCRunAllCases;
  connect(mGeneratorTimer, SIGNAL(timeout()), this,
          SLOT(generatorTimerTimeOutSlot()));
}

void TestDataGen::startNextCase(testcases testCaseNumber) {
  TTPLOG_FUNCTIONSCOPE;
  const int timerIntervalMs = 1000;
  mGeneratorTimer->start(timerIntervalMs);
  mTestCaseChangeTimer->restart();

  enableAutoLayout();
  loadBackGroundImage("");

  /* Change this number for controlling what case to run */
  if (testCaseNumber == TCRunAllCases) {
    int t = static_cast<int>(testCaseNumber);
    t++;
    mTestCaseNumber = static_cast<testcases>(t);

    if (mTestCaseNumber >= TCLastLine) {
      mTestCaseNumber = TCfixedCoordSteadyState;
    }
    if (mTestCaseNumber >= TCfixedCoordSteadyState) {
      mTestCaseNumber = TCRunAllCases;
    }
  } else {
    if (testCaseNumber >= TCLastLine) {
      mTestCaseNumber = TCfixedCoordSteadyState;
    } else {
      mTestCaseNumber = testCaseNumber;
    }
  }

  if (mTestCaseNumber < 0 || mTestCaseNumber >= TCLastLine) {
    qFatal("pick valid test");
  }

  processTestCaseInitialStep((TestDataGen::testcases)mTestCaseNumber);
}

void TestDataGen::generatorTimerTimeOutSlot() {
  TTPLOG_FUNCTIONSCOPE;

  if (mTestCaseNumber == TCRunAllCases) {
    if (mTestCaseChangeTimer->elapsed() > singletestTimeMsec) {
      startNextCase(TCRunAllCases);
      mTestCaseChangeTimer->restart();
    }
  }
  processTestCaseTimerStep((TestDataGen::testcases)mTestCaseNumber);
}

void TestDataGen::processTestCaseInitialStep(
    TestDataGen::testcases caseNumber) {
  TTPLOG_FUNCTIONSCOPE;

  switch (caseNumber) {
  case TCfixedCoordSteadyState:
    TCfixedCoordSteadyStateInitial();
    break;
  case TCAddAndRemoveLinks:
    TCAddAndRemoveLinksInitial();
    break;
  case TCAddAndRemoveNodes:
    TCAddAndRemoveNodesInitial();
    break;
  case TCColorLinksAndNodes:
    TCColorLinksAndNodesInitial();
    break;
  default:
    qFatal("unknown testcase");
    break;
  }
}

void TestDataGen::processTestCaseTimerStep(TestDataGen::testcases caseNumber) {
  TTPLOG_FUNCTIONSCOPE;

  switch (caseNumber) {
  case TCfixedCoordSteadyState:
    TCfixedCoordSteadyStateTimer();
    break;
  case TCAddAndRemoveLinks:
    TCAddAndRemoveLinksTimer();
    break;
  case TCAddAndRemoveNodes:
    TCAddAndRemoveNodesTimer();
    break;
  case TCColorLinksAndNodes:
    TCColorLinksAndNodesTimer();
    break;
  default:
    qFatal("unknown testcase");
    break;
  }
}

QString TestDataGen::createTestNodeName(int k) {
  return QString("testItem-%1").arg(k);
}

void TestDataGen::TCfixedCoordSteadyStateInitial() {
  TTPLOG_FUNCTIONSCOPE;
  clearNodes();

  for (int k = 0; k < DefaultTestItemsCount; k++) {
    QString myNode = createTestNodeName(k);
    createNode(myNode);
  }
  int y = 0;
  int added = 0;
  int edge = defaultEdge;
  int inc = qrand() % edge;

  while (added < DefaultTestItemsCount) {
    for (int x = 0; x < defaultColumnCount; x++) {
      QString myNode = createTestNodeName(added);
      inc = qrand() % edge;
      setNodeCoord(myNode, x * edge + inc, y * edge + inc);
      added++;
    }
    y++;
  }
  /* Create some links */
  addRemoveLinks(0, DefaultTestItemsCount);
}

void TestDataGen::TCfixedCoordSteadyStateTimer() {
  TTPLOG_FUNCTIONSCOPE;

  for (int k = 0; k < DefaultTestItemsCount; k++) {
    QString myNode = createTestNodeName(k);
    createNode(myNode);
  }

  int edge = defaultEdge;
  int inc = (qrand() % edge) * 0.1;
  int y = 0;
  int added = 0;

  while (added < DefaultTestItemsCount) {
    for (int x = 0; x < defaultColumnCount; x++) {
      QString myNode = createTestNodeName(added);
      inc = qrand() % edge;
      setNodeCoord(myNode, x * edge + inc, y * edge + inc);
      added++;
    }
    y++;
  }
}

void TestDataGen::CreateFixesPosNode(QString NodeName, int x, int y, int z,
                                     QString nodeColor) {
  createNode(NodeName);
  setNodeCoord(NodeName, x, y);
  setNodeSize(NodeName, z);
  setNodeColor(NodeName, nodeColor);
}

void TestDataGen::TCAddAndRemoveLinksInitial() {
  TTPLOG_FUNCTIONSCOPE;
  clearNodes();
#if 1
  loadBackGroundImage("testImages/testImage-1000x1000.png");

  CreateFixesPosNode(createTestNodeName(0), 333, 1000 - 922, 3, "red");
  CreateFixesPosNode(createTestNodeName(1), 333, 1000 - 840, 3, "green");
  CreateFixesPosNode(createTestNodeName(2), 333, 1000 - 702, 3, "blue");
  CreateFixesPosNode(createTestNodeName(3), 333, 1000 - 394, 3, "cyan");
  CreateFixesPosNode(createTestNodeName(4), 581, 1000 - 822, 3, "gray");

  //  setNodeLabel(createTestNodeName(0), "label1");
  //  setNodeLabel(createTestNodeName(1), "label2");
  //  setNodeLabel(createTestNodeName(2), "label3");
  //  setNodeLabel(createTestNodeName(3), "label4");
  //  setNodeLabel(createTestNodeName(4), "label5");

  addlink(createTestNodeName(0), createTestNodeName(1));
  addlink(createTestNodeName(1), createTestNodeName(2));
  addlink(createTestNodeName(2), createTestNodeName(3));
  addlink(createTestNodeName(3), createTestNodeName(4));

  addlink(createTestNodeName(4), createTestNodeName(3));
  addlink(createTestNodeName(2), createTestNodeName(4));
#else
  for (int k = 0; k < DefaultTestItemsCount; k++) {
    QString myNode = createTestNodeName(k);
    createNode(myNode);
  }
  int y = 0;
  int added = 0;
  int edge = defaultEdge;
  int inc = qrand() % edge;

  while (added < DefaultTestItemsCount) {
    for (int x = 0; x < defaultColumnCount; x++) {
      QString myNode = createTestNodeName(added);
      inc = qrand() % edge;
      setNodeCoord(myNode, x * edge + inc, y * edge + inc);
      added++;
    }
    y++;
  }
  /* Create some links */
  addRemoveLinks(0, DefaultTestItemsCount);
#endif

  mTestRecalcTimer->restart();
}

void TestDataGen::addRemoveLinks(int removeLinkCount, int addLinkCount) {
  int counter = 0;
  /* remove */
  while (mLinks->size() > 0 && counter < removeLinkCount) {

    int k1;
    k1 = (qrand() % mLinks->keys().size()) - 1;

    if (k1 < 0) {
      k1 = 0;
    }

    QString linkKey = mLinks->keys().at(k1);

    if (mLinks->contains(linkKey) == true) {
      removelink(mLinks->value(linkKey).label1, mLinks->value(linkKey).label2);
      mLinks->remove(linkKey);
      counter++;
    }
  }

  /* Add */
  counter = 0;
  while (counter < addLinkCount) {
    int k1 = 0;
    int k2 = 0;
    while (k1 == k2) {
      k1 = qrand() % DefaultTestItemsCount;
      k2 = qrand() % DefaultTestItemsCount;
    }
    QString linkKey = createTestNodeName(k1) + createTestNodeName(k2);
    if (mLinks->contains(linkKey) == false) {
      NodeLink n;
      n.label1 = createTestNodeName(k1);
      n.label2 = createTestNodeName(k2);
      n.linkKey = linkKey;

      addlink(n.label1, n.label2);
      setLinkColor(n.label1, n.label2, "red");

      mLinks->insert(linkKey, n);
      counter++;
    }
  }
}

void TestDataGen::StartTestCaseSlot(TestDataGen::testcases caseNumber) {
  startNextCase(caseNumber);
}

void TestDataGen::StopTestCaseExecutionSlot() { mGeneratorTimer->stop(); }

void TestDataGen::TCAddAndRemoveLinksTimer() {
  TTPLOG_FUNCTIONSCOPE;

  const int defaultReLayoutTimeMs = 1000;
  if (mTestRecalcTimer->elapsed() > defaultReLayoutTimeMs) {
    mTestRecalcTimer->restart();
  }

  int k1 = 0;
  int k2 = 0;

  k1 = qrand() % 5;
  k2 = qrand() % 5;

  setNodeColor(createTestNodeName(k1), "red");
  setNodeColor(createTestNodeName(k2), "green");
  addRemoveLinks(1, 1);
}

void TestDataGen::TCAddAndRemoveNodesInitial() {
  TTPLOG_FUNCTIONSCOPE;
  clearNodes();

  for (int k = 0; k < DefaultTestItemsCount; k++) {
    QString myNode = createTestNodeName(k);
    createNode(myNode);
  }
  /* Create some links. Energy based layout does not work otherwise */
  addRemoveLinks(DefaultTestItemsCount, DefaultTestItemsCount);
}

void TestDataGen::TCAddAndRemoveNodesTimer() {

  TTPLOG_FUNCTIONSCOPE;
  int addNodeCount;
  int removeNodeCount;

  if (mNodes->size() > 0) {
    /* remove one by one */
    addNodeCount = 0;
    removeNodeCount = 1;
  } else {
    /* add DefaultTestItemsCount */
    addNodeCount = DefaultTestItemsCount;
    removeNodeCount = 0;
  }

  int counter = 0;
  /* remove */
  while (mNodes->size() > 0 && counter < removeNodeCount) {

    int k1;
    k1 = (qrand() % mNodes->keys().size()) - 1;

    if (k1 < 0) {
      k1 = 0;
    }

    QString nodeKey = mNodes->keys().at(k1);

    if (mNodes->contains(nodeKey) == true) {
      removeNode(nodeKey);
      mNodes->remove(nodeKey);
      counter++;
    }
  }

  /* Add */
  counter = 0;
  while (counter < addNodeCount) {
    int k = 0;
    k = mNodes->size() + 1;
    QString nodeKey = createTestNodeName(k);
    if (mNodes->contains(nodeKey) == false) {
      createNode(nodeKey);
      mNodes->insert(nodeKey, nodeKey);
      counter++;
    }
  }
}

void TestDataGen::TCColorLinksAndNodesInitial() {
  TTPLOG_FUNCTIONSCOPE;
  clearNodes();

  for (int k = 0; k < DefaultTestItemsCount; k++) {
    QString myNode = createTestNodeName(k);
    createNode(myNode);
    setNodeColor(myNode, "yellow");
  }

  addRemoveLinks(0, DefaultTestItemsCount);

  //   relayout();
}

void TestDataGen::TCColorLinksAndNodesTimer() {

  const qreal minSize = 0.3;

  for (int z = 0; z < DefaultTestItemsCount; z++) {

    int colorANode = qrand() % DefaultTestItemsCount;
    int colorBNode = qrand() % DefaultTestItemsCount;
    int colorCNode = qrand() % DefaultTestItemsCount;

    /* Nodes */
    QString myNode = createTestNodeName(colorANode);
    setNodeColor(myNode, "blue");

    setNodeSize(myNode,
                minSize + (qreal)colorANode / (qreal)DefaultTestItemsCount);

    myNode = createTestNodeName(colorBNode);
    setNodeColor(myNode, "red");
    setNodeSize(myNode,
                minSize + (qreal)colorANode / (qreal)DefaultTestItemsCount);

    /* Links */
    colorANode = qrand() % DefaultTestItemsCount;
    colorBNode = qrand() % DefaultTestItemsCount;

    qreal linkValue = ((qreal)colorANode / (qreal)DefaultTestItemsCount) * 10.0;

    setLinkValue(createTestNodeName(colorANode), createTestNodeName(colorBNode),
                 linkValue);

    colorANode = qrand() % DefaultTestItemsCount;
    colorBNode = qrand() % DefaultTestItemsCount;

    linkValue = 1.0;

    setLinkValue(createTestNodeName(colorANode), createTestNodeName(colorBNode),
                 linkValue);
  }
  addRemoveLinks(1, 1);
}

void TestDataGen::addlink(QString label1, QString label2) {
  // TTPLOG_STRING(QString("nodes;addlink;%1;%2").arg(label1).arg(label2));
  QString msg = QString("nodes;addlink;%1;%2").arg(label1).arg(label2);
  addTimeStampAndSendMessage(msg);
}

void TestDataGen::removelink(QString label1, QString label2) {
  // TTPLOG_STRING(QString("nodes;removelink;%1;%2").arg(label1).arg(label2));
  QString msg = QString("nodes;removelink;%1;%2").arg(label1).arg(label2);
  addTimeStampAndSendMessage(msg);
}

void TestDataGen::clearNodes() {
  // TTPLOG_STRING(QString("nodes;clear");
  QString msg("nodes;clear");
  addTimeStampAndSendMessage(msg);
}

void TestDataGen::createNode(QString label) {
  // TTPLOG_STRING(QString("nodes;create;%1").arg((int)p));
  QString msg = QString("nodes;create;%1").arg(label);
  addTimeStampAndSendMessage(msg);
}

void TestDataGen::removeNode(QString label) {
  // TTPLOG_STRING(QString("nodes;removenode;%1").arg((int)p));
  QString msg = QString("nodes;removenode;%1").arg(label);
  addTimeStampAndSendMessage(msg);
}

void TestDataGen::setNodeCoord(QString label, qreal x, qreal y) {
  /*TTPLOG_STRING(QString("nodes;setcoord;%1;%2;%3").arg((int)p)
                .arg(QRect(x,y,edge,edge).center().x())
                .arg(QRect(x,y,edge,edge).center().y()));*/
  QString msg = QString("nodes;setcoord;%1;%2;%3")
                    .arg(label)
                    .arg(QString::number(x))
                    .arg(QString::number(y));
  addTimeStampAndSendMessage(msg);
}

void TestDataGen::relayout() {
  // TTPLOG_STRING(QString("nodes;relayout");
  QString msg = QString("nodes;relayout");
  addTimeStampAndSendMessage(msg);
}

void TestDataGen::enableAutoLayout() {
  // TTPLOG_STRING(QString("nodes;enableautolayout");
  QString msg = QString("nodes;enableautolayout");
  addTimeStampAndSendMessage(msg);
}

void TestDataGen::disableAutoLayout() {
  // TTPLOG_STRING(QString("nodes;disableautolayout");
  QString msg = QString("nodes;disableautolayout");
  addTimeStampAndSendMessage(msg);
}

void TestDataGen::setNodeColor(QString label, QString QtNamedColor) {
  // TTPLOG_STRING(QString("nodes;setnodecolor;%1;%2");
  QString msg =
      QString("nodes;setnodecolor;%1;%2").arg(label).arg(QtNamedColor);
  addTimeStampAndSendMessage(msg);
}

void TestDataGen::setNodeValue(QString label, qreal nodeValue) {
  // TTPLOG_STRING(QString("nodes;setnodevalue;%1;%2");
  QString msg = QString("nodes;setlinkcolor;%1;%2").arg(label).arg(nodeValue);
  addTimeStampAndSendMessage(msg);
}

void TestDataGen::setNodeSize(QString label, qreal nodeSize) {
  // TTPLOG_STRING(QString("nodes;setnodesize;%1;%2");
  QString msg = QString("nodes;setnodesize;%1;%2").arg(label).arg(nodeSize);
  addTimeStampAndSendMessage(msg);
}

void TestDataGen::setNodeLabel(QString label, QString nodeText) {
  // TTPLOG_STRING(QString("nodes;setnodelabel;%1;%2");
  QString msg = QString("nodes;setnodelabel;%1;%2").arg(label).arg(nodeText);
  addTimeStampAndSendMessage(msg);
}

void TestDataGen::setLinkColor(QString label1, QString label2,
                               QString QtNamedColor) {
  // TTPLOG_STRING(QString("nodes;setlinkcolor;%1;%2;%3");
  QString msg = QString("nodes;setlinkcolor;%1;%2;%3")
                    .arg(label1)
                    .arg(label2)
                    .arg(QtNamedColor);
  addTimeStampAndSendMessage(msg);
}

void TestDataGen::setLinkValue(QString label1, QString label2,
                               qreal linkValue) {
  // TTPLOG_STRING(QString("nodes;setlinkvalue;%1;%2;%3");
  QString msg = QString("nodes;setlinkvalue;%1;%2;%3")
                    .arg(label1)
                    .arg(label2)
                    .arg(linkValue);
  addTimeStampAndSendMessage(msg);
}

void TestDataGen::loadBackGroundImage(QString pathToImage) {
  // TTPLOG_STRING(QString("nodes;loadBackGroundImage;%1").arg(QString
  // pathToImage);
  QString msg = QString("nodes;loadBackGroundImage;%1").arg(pathToImage);
  addTimeStampAndSendMessage(msg);
}

void TestDataGen::addTimeStampAndSendMessage(QString msg) {
  QString noderMsg;
  QString timestamp = QDateTime::currentDateTime().toString(Qt::ISODate);

#ifdef TIMETOPICLOGGERAPI
#ifndef TTPLOGGING
#error define TTPLOGGING before running the test
#endif
  TTPLOG_STRING(msg);
#else
  noderMsg = QString("%1;%2\n").arg(timestamp).arg(msg);
  emit testDataMessageAvailableSignal(noderMsg);
#endif
}

#ifndef TESTDATAGEN_H
#define TESTDATAGEN_H

#include "timetopiclogger.h"
#include "worker.h"
#include <QElapsedTimer>
#include <QHash>
#include <QObject>
#include <QString>
#include <QTimer>

/* If defined, test traces are sent via socket to TimeToPic Logger that then
 * routes them back via official connection. */
//#define TIMETOPICLOGGERAPI

class TestDataGen : public worker {
  Q_OBJECT

public:
  enum testcases {
    TCRunAllCases,
    TCfixedCoordSteadyState,
    TCAddAndRemoveLinks,
    TCAddAndRemoveNodes,
    TCColorLinksAndNodes,
    TCLastLine // keep this last in the list
  };

  TestDataGen();

  virtual void InitThis();
  void startNextCase(testcases testCaseNumber);
  QString createTestNodeName(int k);
  void addRemoveLinks(int removeLinkCount, int addLinkCount);
  void CreateFixesPosNode(QString NodeName, int x, int y, int z,
                          QString nodeColor);
public slots:
  void StartTestCaseSlot(TestDataGen::testcases caseNumber);
  void StopTestCaseExecutionSlot();
signals:
  void testDataMessageAvailableSignal(QString msg);
private slots:
  void generatorTimerTimeOutSlot();

private:
  struct NodeLink {
    QString linkKey;
    QString label1;
    QString label2;
  };

  void processTestCaseInitialStep(testcases caseNumber);
  void processTestCaseTimerStep(testcases caseNumber);

  /* Testcase handlers */
  void TCfixedCoordSteadyStateInitial();
  void TCfixedCoordSteadyStateTimer();
  void TCAddAndRemoveLinksInitial();
  void TCAddAndRemoveLinksTimer();
  void TCAddAndRemoveNodesInitial();
  void TCAddAndRemoveNodesTimer();
  void TCColorLinksAndNodesInitial();
  void TCColorLinksAndNodesTimer();

  /* API functions for making testst */
  void addlink(QString label1, QString label2);
  void removelink(QString label1, QString label2);
  void clearNodes();
  void createNode(QString label);
  void removeNode(QString label);
  void setNodeCoord(QString label, qreal x, qreal y);
  void relayout();
  void enableAutoLayout();
  void disableAutoLayout();
  void setNodeColor(QString label, QString QtNamedColor);
  void setNodeValue(QString label, qreal nodeValue);
  void setNodeSize(QString label, qreal nodeSize);
  void setNodeLabel(QString label, QString nodeText);

  void setLinkColor(QString label1, QString label2, QString QtNamedColor);
  void setLinkValue(QString label1, QString label2, qreal linkValue);

  void loadBackGroundImage(QString pathToImage);
  void addTimeStampAndSendMessage(QString msg);

  QTimer *mGeneratorTimer;
  QElapsedTimer *mTestRecalcTimer;
  QElapsedTimer *mTestCaseChangeTimer;
  testcases mTestCaseNumber;

  QHash<QString, NodeLink> *mLinks;
  QHash<QString, QString> *mNodes;
};

#endif // TESTDATAGEN_H

<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="TimeToPic logger example code" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1422894467319"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="3"/>
<node TEXT="Perf counters" POSITION="left" ID="ID_1027539244" CREATED="1422857584489" MODIFIED="1422894514571">
<edge COLOR="#00ff00"/>
<node TEXT="OS" ID="ID_1191266153" CREATED="1422857592983" MODIFIED="1422857593820">
<node TEXT="Windows" ID="ID_630916689" CREATED="1422898772783" MODIFIED="1422898774411">
<node TEXT="Process Mem use" ID="ID_1438474512" CREATED="1422857601136" MODIFIED="1422898789900">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="Process CPU use" ID="ID_895857512" CREATED="1422857594576" MODIFIED="1422898790435">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node TEXT="Linux" ID="ID_954131623" CREATED="1422898793734" MODIFIED="1422898795124"/>
</node>
<node TEXT="ENABLE macro" ID="ID_1480419704" CREATED="1422857611063" MODIFIED="1422898798218">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="Interval" ID="ID_307933475" CREATED="1422857630023" MODIFIED="1422898800674">
<icon BUILTIN="button_ok"/>
<node TEXT="1sec" ID="ID_1302979631" CREATED="1422857635151" MODIFIED="1422898801763">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="Qt event loop (timer)" ID="ID_1865015878" CREATED="1422857640791" MODIFIED="1422898802195">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
</node>
</map>

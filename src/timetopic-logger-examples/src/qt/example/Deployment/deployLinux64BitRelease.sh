#!/bin/bash

#From Qt Creator #1 should be %{buildDir} and %2 %{sourceDir} %3 "<packageName>"
SOURCEBINARYPATH=$1
PROJECTROOT=$2
PACKAGENAME=$3

#make sure that you are compiling with versio here 
#make sure that path is same both deploytLinux64BitRelease.sh and bundle-linuxUbuntu-Libs.sh
LINUXQTGCC_64_LOC=/home/timetopic/Qt5.2.1/5.2.1/gcc_64

DEPLOYPATH=$PROJECTROOT/Deployment
DEPLOYTOOLPATHLINUX=$DEPLOYPATH"/LinuxUbuntu/64bitRelease"
SOURCEPATH=$PROJECTROOT

BINARYNAME=TtpLoggerExample

ZIPNAME=$BINARYNAME"-"$PACKAGENAME

coolerzip() { cd "${1%/*}" && zip -r "${1##*/}" "${1##*/}" && cd -; }

#Before invoking this, do compilation from Qt creator

#Perform clearing target folder root


if [ -f $DEPLOYTOOLPATHLINUX/$ZIPNAME.zip ];
then
   rm $DEPLOYTOOLPATHLINUX/$ZIPNAME.zip
fi

rm -rf $DEPLOYTOOLPATHLINUX
mkdir $DEPLOYTOOLPATHLINUX

#Perform copy of files
#Binary 
cp $SOURCEBINARYPATH/$BINARYNAME $DEPLOYTOOLPATHLINUX

#linux qt
#lets now copy plugings and libs since we assume that 
#system has Qt in place.
qmldir=$DEPLOYTOOLPATHLINUX/Qt5QmlWindows
mkdir $qmldir
cp -r $LINUXQTGCC_64_LOC/qml/* $qmldir

#DOC

#Example files


#Perform zippping 
cd $DEPLOYTOOLPATHLINUX
coolerzip $DEPLOYTOOLPATHLINUX
mv $DEPLOYTOOLPATHLINUX".zip" $DEPLOYPATH/$ZIPNAME".zip"

#Create MD5 text file
echo MD5 generation:
echo $DEPLOYPATH/$ZIPNAME.zip
md5sum $DEPLOYPATH/$ZIPNAME.zip > $DEPLOYPATH/$ZIPNAME"_md5.txt"
echo MD5 check:
md5sum -c $DEPLOYPATH/$ZIPNAME"_md5.txt"
md5sum $DEPLOYPATH/$ZIPNAME.zip
echo "Done deployment."

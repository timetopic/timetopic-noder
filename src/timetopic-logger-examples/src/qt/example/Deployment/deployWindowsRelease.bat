@echo off

::From Qt Creator #1 should be %{buildDir} and %2 %{sourceDir} %3 "<packageName>"
set SOURCEBINARYPATH=%1
set PROJECTROOT=%2
set PACKAGENAME=%3

set ZIPNAME=TimeToPiclogger-%PACKAGENAME%

set PROJECT_FOLDER=%PROJECTROOT%
set DEPLOYMENTROOT=%PROJECT_FOLDER%\deployment
set DEPLOYMENTWINDOWS=%DEPLOYMENTROOT%\Windows


:: Lets use same dlls than in TimeToPic
set DLLPATH=C:\Users\Erkki\Documents\Projektit\Ohjelmointi\Timetool\Sources\TimeToPic\Deployment\windows-dlls-Qt5.5.0-MSVC2010-32bit

set SHADOWBUILDDIR=%SOURCEBINARYPATH%



set ZIPTOOL="C:\Program Files\7-Zip\7z.exe"

::Set paths 

::Clean
rmdir /S /Q %DEPLOYMENTWINDOWS%\
mkdir %DEPLOYMENTWINDOWS%

::Copy exe 
xcopy %SHADOWBUILDDIR%\release\TtpLoggerExample.exe %DEPLOYMENTWINDOWS% /e /i /y

::Copy dlls
echo copy dll's
xcopy %DLLPATH%\*.* %DEPLOYMENTWINDOWS% /e /i /y

::make package
del %DEPLOYMENTROOT%\%ZIPNAME%.zip
%ZIPTOOL% a -r -tzip %DEPLOYMENTROOT%\%ZIPNAME% %DEPLOYMENTWINDOWS%

::md5
echo "Do MD5"
::cd %DEPLOYMENTROOT%
md5 %DEPLOYMENTROOT%\%ZIPNAME%.zip > %DEPLOYMENTROOT%\%ZIPNAME%_md5.txt

echo "Done"

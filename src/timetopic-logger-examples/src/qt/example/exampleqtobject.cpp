  /* The MIT License (MIT)

Copyright (c) 2015 Herwood Technologies oy

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#include <QDebug>
#include <QtDebug>

#include "exampleqtobject.h"
#include "../timetopiclogger.h"

ExampleQtObject::ExampleQtObject(QObject *parent) :
    QObject(parent), m_myTimerSlotCallCount(0)
{
    mTimeSinceStart.start();
}

void ExampleQtObject::StartTestTimer()
{
    TTPLOG_EVENTSCOPE("C++ Timer Test timer active");

    connect(&m_myTimer, SIGNAL(timeout()),this,SLOT(TimerTimeOut()));

    const int defaultTimeOutMs = 1000;
    m_myTimer.start(defaultTimeOutMs);
}

void ExampleQtObject::TimerTimeOut()
{
    /* Function scope macro that generates entry and exit */
    TTPLOG_FUNCTIONSCOPE;

    /* Example of delta value. Move mouse top of channel and use "k" letter show graph
       different way. */
    TTPLOG_VALUE("C++ TimerTimeOutCount",m_myTimerSlotCallCount);
    m_myTimerSlotCallCount++;

    /* Example of delta value. Move mouse top of channel and use TimeToPic math "sum"
     * to see cumulative sum */
    TTPLOG_VALUE("C++ TimerTimeOutCountDelta",1);

    /* Examples of various state machines. Use TimeToPic color palette cycling ('c' letter)
     * for seeing different colors */
    if (m_myTimerSlotCallCount % 2 == 0 ) {
        TTPLOG_STATE("C++ MyStateMachine","State A");
    }
    else if (m_myTimerSlotCallCount % 3 == 0) {
        TTPLOG_STATE("C++ MyStateMachine","State B");
    }
    else {
        TTPLOG_STATE("C++ MyStateMachine","State C");
    }

    qDebug() << "Example debug message C++ timer timeout slot";
    qWarning() << "Example qWarning level message ";
    qCritical() << "Example qCritical";

#if 0
    const int maxRunTimeMs = 60 * 1000;
    if (mTimeSinceStart.elapsed()>maxRunTimeMs) {
        qCritical() << "Now app will crash due of call of qFatal";
        qFatal("Eaxample qFatal for mTimeSinceStart.elapsed()>maxRunTimeMs");
    }
#endif
}

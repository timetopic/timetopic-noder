/* The MIT License (MIT)

Copyright (c) 2015 Herwood Technologies oy

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

import QtQuick 2.2
import QtQuick.Controls 1.1

ApplicationWindow {
    id: myApp
    visible: true
    width: 640
    height: 480
    title: qsTr("Logger Qt/QML example")

    Component.onCompleted: {

        // ### This adds an event into log
        ttplogger.logEvent("QML Window", true)        

    }
    Component.onDestruction: {

        // ### This stops an event in current log
        ttplogger.logEvent("QML Window", false)
    }

    property int counter: 0


    Row {
        id: myRow

        Column {

            Rectangle {
                width: myApp.width/2
                height: myApp.height/2
                property string boxName: "QML Mouse on box1"
                color:"white"

                MouseArea {
                    hoverEnabled: true
                    anchors.fill:  parent
                    onEntered: {
                        parent.color="green"
                        ttplogger.logState("QML mouseLocState",parent.boxName)
                        ttplogger.logEvent(parent.boxName,true)


                    }
                    onExited: {
                        parent.color="white"
                        ttplogger.logEvent(parent.boxName,false)
                    }
                    onMouseXChanged: {
                        ttplogger.logValue(parent.boxName+"mouseX",mouseX)
                    }
                    onMouseYChanged: {
                        ttplogger.logValue(parent.boxName+"mouseY",mouseY)
                    }
                }
            }
            Rectangle {
                width: myApp.width/2
                height: myApp.height/2
                property string boxName: "QML Mouse on box3"
                color:"lightgreen"
                MouseArea {
                    hoverEnabled: true
                    anchors.fill:  parent
                    onEntered: {
                        parent.color="green"
                        ttplogger.logState("QML mouseLocState",parent.boxName)
                        ttplogger.logEvent(parent.boxName,true)
                    }
                    onExited: {
                        parent.color="lightgreen"
                        ttplogger.logEvent(parent.boxName,false)
                    }
                }
            }

        }

        Column {

            Rectangle {
                width: myApp.width/2
                height: myApp.height/2
                color:"lightgreen"
                property string boxName: "QML Mouse on box2"
                MouseArea {
                    hoverEnabled: true
                    anchors.fill:  parent
                    onEntered: {
                        parent.color="green"
                        ttplogger.logState("QML mouseLocState",parent.boxName)
                        ttplogger.logEvent(parent.boxName,true)
                    }
                    onExited: {
                        parent.color="lightgreen"
                        ttplogger.logEvent(parent.boxName,false)
                    }
                }
            }
            Rectangle {
                width: myApp.width/2
                height: myApp.height/2
                color:"white"
                property string boxName: "QML Mouse on box4"
                MouseArea {
                    hoverEnabled: true
                    anchors.fill:  parent
                    onEntered: {
                        parent.color="green"
                        ttplogger.logState("QML mouseLocState",parent.boxName)
                        ttplogger.logEvent(parent.boxName,true)
                    }
                    onExited: {
                        parent.color="white"
                        ttplogger.logEvent(parent.boxName,false)
                    }
                }
            }
        }
    }



    Text {
        text: "Note! Make sure that TimeToPicLogger application is running in localhost!"
        font.pixelSize: 14
    }

    Rectangle {
        anchors.centerIn: parent
        width: parent.width * 0.3
        height: 40
        border.width: 1
        color: "#77aa77"
        Text {
            anchors.centerIn: parent
            text: "Click me!<br> Counter:" + counter
        }


        MouseArea {
            anchors.fill: parent
            onClicked: {
                // ### This adds a value into log
                ttplogger.logValue("QML counter", counter);
                counter = counter + 1;

                ttplogger.logString("nodes;clear")
                ttplogger.logString("nodes;create;box1")
                ttplogger.logString("nodes;create;box2")
                ttplogger.logString("nodes;create;box3")
                ttplogger.logString("nodes;create;box4")

                ttplogger.logString("nodes;addlink;box1;box2")
                ttplogger.logString("nodes;addlink;box2;box3")
                ttplogger.logString("nodes;addlink;box3;box4")
                ttplogger.logString("nodes;addlink;box4;box1")
                ttplogger.logString("nodes;enableautolayout")
            }
        }
    }

}

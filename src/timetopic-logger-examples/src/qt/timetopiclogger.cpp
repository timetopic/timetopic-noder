/* The MIT License (MIT)

Copyright (c) 2015 Herwood Technologies oy

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#include <QProcess>
#include <QFile>
#include <QTextStream>
#include <QStringList>
#include <QDateTime>
#include <stdio.h>
#include <stdlib.h>

#include "timetopiclogger.h"

#ifdef Q_OS_LINUX
#include <unistd.h>
#endif



TimeToPicLogger *TimeToPicLogger::m_instance = NULL;


void myMessageOutput(QtMsgType type, const QMessageLogContext & /*context */, const QString &msg)
{
    QByteArray myLocalMsg = msg.toLocal8Bit();

    QString msgSuffix = QString("%1").arg(myLocalMsg.constData());
    QString myMsg;

    switch (type) {
    case QtDebugMsg:
        myMsg = QString("%1").arg(msgSuffix);
        TTPLOG_STRING(myMsg);
        break;
    case QtWarningMsg:
        myMsg = QString("%1%2").arg("Warning: ").arg(msgSuffix);
        TTPLOG_STRING(myMsg);
        break;
    case QtCriticalMsg:
        myMsg = QString("%1%2").arg("Critical: ").arg(msgSuffix);
        TTPLOG_STRING(myMsg);
        break;
    case QtFatalMsg:
        myMsg = QString("%1%2").arg("Fatal: ").arg(msgSuffix);
        TTPLOG_STRING(myMsg);
        TTPLOG_EVENT("qFatal",true); // generate event for seq analysis
        abort();
    default:
        myMsg = QString("%1%2").arg("").arg(msgSuffix);
        TTPLOG_STRING(myMsg);
        break;
    }
}

TimeToPicLogger::TimeToPicLogger(): m_psMemoryUSed(0)
{

#ifdef Q_OS_WIN32
    m_ftPrevSysKernel.dwHighDateTime = 0;
    m_ftPrevSysKernel.dwLowDateTime = 0;
    m_ftPrevSysUser.dwHighDateTime = 0;
    m_ftPrevSysUser.dwLowDateTime = 0;
    m_ftPrevProcKernel.dwHighDateTime = 0;
    m_ftPrevProcKernel.dwLowDateTime = 0;
    m_ftPrevProcUser.dwHighDateTime = 0;
    m_ftPrevProcUser.dwLowDateTime = 0;
#endif

    m_prevProcessTime = 0;
    m_prevTotalTime = 0;


    m_elapsed.start();
}


TimeToPicLogger *TimeToPicLogger::instance()
{
    static QMutex instanceMutex;
    QMutexLocker locker(&instanceMutex);
    Q_UNUSED(locker);

    if(m_instance == NULL)
    {
        m_instance = new TimeToPicLogger();
        m_instance->start();                
    }
    return m_instance;
}

void TimeToPicLogger::run()
{
    LogStreamThread thread;
    connect(this, SIGNAL(newLogRow(QString)), &thread, SLOT(writeRow(QString)));
    connect(&m_performanceCounterTimer, SIGNAL(timeout()),this,SLOT(performanceCounterTimerSlot()));
    exec();
}

void TimeToPicLogger::stop()
{
    /* Stop possible performance timer */
    m_performanceCounterTimer.stop();

    exit();
    const int exitTimeTimeOutMs = 500; // wait for maximum of 500ms for run function to exit
    wait(exitTimeTimeOutMs);
}

void TimeToPicLogger::logString(const QString&  text)
{
    QMutexLocker locker(&m_mutex);
    Q_UNUSED(locker);

    emit newLogRow(getTimestamp() + ";" + text);
}

void TimeToPicLogger::logEvent(const QString&  channel, EventState eventState)
{
    // mutex is here just to make sure that items are in timely order
    QMutexLocker locker(&m_mutex);
    Q_UNUSED(locker);
    QString stateString;
    switch(eventState)
    {
    case EventStart:
        stateString = "start";
        break;
    case EventStop:
        stateString = "stop";
        break;
    }

    QString concat = getTimestamp() + ";event;" + stateString + ";" + channel;
    emit newLogRow(concat);
}

void TimeToPicLogger::logEvent(const QString&  channel, bool startEvent)
{
    if(startEvent)
    {
        logEvent(channel, EventStart);
    }
    else
    {
        logEvent(channel, EventStop);
    }
}

void TimeToPicLogger::logState(const QString&  channel, const QString&  state)
{
    // mutex is here just to make sure that items are in timely order
    QMutexLocker locker(&m_mutex);
    Q_UNUSED(locker);

    QString concat = getTimestamp() + ";state;" + state + ";" + channel;
    emit newLogRow(concat);
}

void TimeToPicLogger::logValue(const QString&  channel, const QString&  value)
{
    // mutex is here just to make sure that items are in timely order
    QMutexLocker locker(&m_mutex);
    Q_UNUSED(locker);

    QString concat = getTimestamp() + ";valueabs;" + value + ";" + channel;
    emit newLogRow(concat);
}

void TimeToPicLogger::enableOsPerfCounters(bool bOn)
{

#ifdef Q_OS_WIN32
    const int performanceCounterIntervalMs =1000;
#else
    /* For linux, make calls interval longer since accessing
     * proc is heavier operation */
    const int performanceCounterIntervalMs =3000;
#endif


    QMutexLocker locker(&m_mutex);
    Q_UNUSED(locker);

    if (bOn==true) {
        m_performanceCounterTimer.start(performanceCounterIntervalMs);
    }
    else {
        m_performanceCounterTimer.stop();
    }
}

void TimeToPicLogger::performanceCounterTimerSlot()
{
    reportCPULoad();
    reportProcessMemUse();
}
#ifdef Q_OS_WIN32
ULONGLONG TimeToPicLogger::delta(const FILETIME& time1, const FILETIME& time2)
  {
    /* Calculate delta between 2 times */

    LARGE_INTEGER a, b;
    a.LowPart = time1.dwLowDateTime;
    a.HighPart = time1.dwHighDateTime;

    b.LowPart = time2.dwLowDateTime;
    b.HighPart = time2.dwHighDateTime;

    return a.QuadPart - b.QuadPart;
 }
#endif

void TimeToPicLogger::reportCPULoad()
{
    qint16 cpuLoadPercent = 0;
#ifdef Q_OS_WIN32

    // Design reference for OS counters
    // http://www.philosophicalgeek.com/2009/01/03/determine-cpu-usage-of-current-process-c-and-c/

    FILETIME ftSysIdle, ftSysKernel, ftSysUser;
    FILETIME ftProcCreation, ftProcExit, ftProcKernel, ftProcUser;

    GetSystemTimes(&ftSysIdle, &ftSysKernel, &ftSysUser);
    GetProcessTimes(GetCurrentProcess(), &ftProcCreation,&ftProcExit, &ftProcKernel, &ftProcUser);

    ULONGLONG deltaSysKernelTime =
            delta(ftSysKernel, m_ftPrevSysKernel);

    ULONGLONG deltaSysUser =
            delta(ftSysUser, m_ftPrevSysUser);

    ULONGLONG deltaProcKernelTime =
            delta(ftProcKernel, m_ftPrevProcKernel);

    ULONGLONG deltaProcUserTime =
            delta(ftProcUser, m_ftPrevProcUser);

    ULONGLONG totalSystemTime =  deltaSysKernelTime + deltaSysUser;

    ULONGLONG totalProcessTime = deltaProcKernelTime + deltaProcUserTime;

    cpuLoadPercent = (qint16)((100.0 * totalProcessTime) / totalSystemTime);

    if (m_ftPrevSysKernel.dwHighDateTime==0 && m_ftPrevSysUser.dwHighDateTime==0
            && m_ftPrevProcKernel.dwHighDateTime==0 && m_ftPrevProcUser.dwHighDateTime==0) {
        cpuLoadPercent=-1;
    }

    m_ftPrevSysKernel = ftSysKernel;
    m_ftPrevSysUser = ftSysUser;
    m_ftPrevProcKernel = ftProcKernel;
    m_ftPrevProcUser = ftProcUser;

#endif

#ifdef Q_OS_LINUX
    // to be added later
    QString pid = getThisProcessPID();
    // VmSize:	  254552 kB
    QString procItem = QString("%1/%2").arg(pid).arg("stat");

    qreal totalTime = getCPUTimeProc();
    const int processTimePosIndexInStat = 14;
    QString procTimeValue= getProcB(processTimePosIndexInStat,procItem);
    bool bOk;
    qreal procTime = procTimeValue.toDouble(&bOk);

    qreal MY_HZ = sysconf(_SC_CLK_TCK);
    qreal multiplier = 10.0; // when multiplying 10, gives same result than top-tool
    if (bOk==true) {
        /* We have valid numbers */
        qreal ratio = (procTime-m_prevProcessTime) / (totalTime-m_prevTotalTime);
#if 0
        qDebug()<<pid << ratio*MY_HZ*multiplier;
#endif
        cpuLoadPercent = ratio*MY_HZ*multiplier;
    }

    m_prevProcessTime = procTime;
    m_prevTotalTime = totalTime;

#endif

    /* Report different cpu use only */
    if (cpuLoadPercent!=m_cpuLoadPercent && cpuLoadPercent>=0) {
        logValue("OS.COUNTER.PS.CPULOAD", QString::number(cpuLoadPercent));
        m_cpuLoadPercent =cpuLoadPercent;
    }


}

void TimeToPicLogger::reportProcessMemUse()
{
    // http://stackoverflow.com/questions/8122277/getting-memory-information-with-qt
    qint64 psMemoryUSed = 0 ;

#ifdef Q_OS_WIN32
    qint64 ProcessID =  QCoreApplication::applicationPid();
    if (ProcessID!=0) {
        HANDLE hProcess;
        PROCESS_MEMORY_COUNTERS pmc;
        hProcess = OpenProcess(  PROCESS_QUERY_INFORMATION |
                                        PROCESS_VM_READ,
                                        FALSE, ProcessID );
        if ( GetProcessMemoryInfo( hProcess, &pmc, sizeof(pmc)) ) {
                psMemoryUSed = pmc.WorkingSetSize;
        }
    }
#endif
#ifdef Q_OS_LINUX
    /* Please note that memory query like this is bit expensive */
    /* Currently vmSize is returned. Tested on Ubuntu */

    QString pid = getThisProcessPID();
    // VmSize:	  254552 kB
    QString procItem = QString("%1/%2").arg(pid).arg("status");
    QString procResult = getProcA("VmSize",procItem).trimmed();

    /* Result is kb */
    QString memAllocKB = procResult.replace(" kB","");
#if 0
    qDebug() << memAllocKB;
#endif
    bool bOk;
    m_psMemoryUSed = memAllocKB.toInt(&bOk,10);
    if ( bOk == false) {
        m_psMemoryUSed =-1;
    }

#endif
    if (psMemoryUSed!=m_psMemoryUSed) {
        /* Report different memory value only */
        logValue("OS.COUNTER.PS.USEDMEMKB", QString::number(psMemoryUSed/1024));
        m_psMemoryUSed = psMemoryUSed;
    }
}

#ifdef Q_OS_LINUX
QString TimeToPicLogger::getProcA(QString procItemKey, QString procItem)
{
    /* Return desired value from proc where items are ordered like:
       SReclaimable:     280308 kB
       SUnreclaim:        47440 kB
       KernelStack:        4120 kB
       PageTables:        40436 kB
       NFS_Unstable:          0 kB
    */
    const QString procPath = "/proc";

    QString ret = "";
    QString fileName(QString("%1/%2").arg(procPath).arg(procItem));

    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        /* Cannot open */
    }
    else {
        QTextStream in(&file);
          QString line = in.readLine();
          while (!line.isNull()) {
              /*    SReclaimable:     280308 kB
                    SUnreclaim:        47440 kB
                    KernelStack:        4120 kB
                    PageTables:        40436 kB
                    NFS_Unstable:          0 kB
                */

              QStringList splits = line.split(":");
              if (splits.size()==2) {
                  QString key = splits.first();
                  if (key == procItemKey ) {
                      ret = splits.last();
                      break;
                  }
              }

              line = in.readLine();
          }
    }
    return ret;
}

QString TimeToPicLogger::getProcB(int nthLineFromProc, QString procItem)
{
    /* Return desired item from proc that are separated by " ":
       2098 (zeitgeist-fts) S 1613 1690 1690 0 -1 4218880 3375 0 39 0 20 12 0 0 20 0 3 0 9709...
       Data is in one long line
    */
    const QString procPath = "/proc";

    QString ret = "";
    QString fileName(QString("%1/%2").arg(procPath).arg(procItem));

    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        /* Cannot open */
    }
    else {
        QTextStream in(&file);
          QString line = in.readLine();
          if (!line.isNull()) {
              QStringList splits = line.split(" ");
              if (splits.size()>=nthLineFromProc) {
              ret = splits.at(nthLineFromProc);
              }
          }
    }
    return ret;
}

qreal TimeToPicLogger::getCPUTimeProc()
{
    // http://wiki.linuxwall.info/doku.php/en:ressources:astuces:cpu_process_usage

    /* /proc/stat
    cpu  102158 30694 44995 655490 37079 2612 0 0 0 0
    cpu0 27795 7251 10926 437187 33911 451 0 0 0 0
    cpu1 27506 6438 10788 73311 357 379 0 0 0 0
    cpu2 24631 7740 13024 71796 1279 965 0 0 0 0
    cpu3 22224 9264 10255 73195 1530 816 0 0 0 0
    intr 4858671 22 12496 0 0 0 0 0 0 1 601 0 0 30262 0 0 0 188 486725 0 0 0 0 0 71 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 12418 352118 305748 17 89 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
    ctxt 14707941
    btime 1423295120
    processes 79308
    procs_running 1
    procs_blocked 0
    softirq 2594503 29 625792 1122 1492 353870 3 1000436 230170 3194 378395 */

    const QString procPath = "/proc";

    qreal ret = -1;
    QString fileName(QString("%1/%2").arg(procPath).arg("stat"));
    QString procItemKey = "cpu";

    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        /* Cannot open */
    }
    else {
        QTextStream in(&file);
          QString line = in.readLine();
          while (!line.isNull()) {

              // cpu  102158 30694 44995 655490 37079 2612 0 0 0 0
              // note 2 spaces after cpu
              QStringList splits = line.split(" ");
              if (splits.size()==12) {
                  QString key = splits.first();
                  if (key == procItemKey ) {

                      for (int k=2;k<splits.size();k++) {
                          bool bOk;
                          ret+=splits.at(k).toDouble(&bOk);
                          if (bOk==false) {
                              ret = -1;
                              break;
                          }
                      }
                      break;
                  }
              }
              line = in.readLine();
          }
    }
    return ret;
}

QString TimeToPicLogger::getThisProcessPID()
{
    QString ret =QString("%1").arg( QCoreApplication::applicationPid() );
    return ret;
}
#endif


QString TimeToPicLogger::getTimestamp()
{
    // use nanosecond resolution for high accuracy timing
#ifdef USEDATETIME
    QDateTime now = QDateTime::currentDateTime();
    #define TIMETOPIC_DATETIME_FORMAT "yyyy/MM/dd hh:mm:ss.zzz" // you need to add msecs using Time-function.
    return now.toString(TIMETOPIC_DATETIME_FORMAT);
#else
    return QString::number(m_elapsed.nsecsElapsed()/1000000000.0,'f',6);
#endif
}


LogStreamThread::LogStreamThread()
{
    mConnectingInProgress = false;
    connect(&mConnSetupTimer,SIGNAL(timeout()),this,SLOT(connectionTryTimerTimeTimeoutSlot()));

    connect(&mSocket,SIGNAL(connected()),this,SLOT(socketConnectedSlot()));
    connect(&mSocket,SIGNAL(disconnected()),this,SLOT(socketDisconnectedSlot()));
    connect(&mSocket,SIGNAL(error(QAbstractSocket::SocketError)),
            this,SLOT(socketErrorSlot(QAbstractSocket::SocketError)));

    connect(&mUDPDiscoverySocket, SIGNAL(readyRead()),this,SLOT(udpSocketReadyReadSlot()));
}

void LogStreamThread::writeRow(QString txt)
{
#ifdef TTPLOGGING
    if(mSocket.state() == QAbstractSocket::ConnectedState) {

        // Flush buffer contents first, if there is anything
        foreach(const QString& str, mBuffer) {
            mSocket.write(str.toUtf8());
            mSocket.write("\r\n");
        }
        mBuffer.clear();

        mSocket.write(txt.toUtf8());
        mSocket.write("\r\n");
    }
    else if(mSocket.state() == QAbstractSocket::UnconnectedState) {
        mBuffer.append(txt);
        // Make sure that buffer will not overgrow!
        while(mBuffer.length() > BUFFER_MAX_LINE_COUNT) {
            mBuffer.pop_front();
        }

        tryConnectionToServer();
    }
#endif
}

void LogStreamThread::connectionTryTimerTimeTimeoutSlot()
{
    mConnSetupTimer.stop();
    if (mSocket.state() == QAbstractSocket::UnconnectedState) {
        connectToServer();
    }
}

void LogStreamThread::socketDisconnectedSlot()
{
    /* Socket connection is lost */
    /* restore system handler */
    qInstallMessageHandler(0);

    /* Restart connection retry */
    mConnectingInProgress = false;
    tryConnectionToServer();
}

void LogStreamThread::socketConnectedSlot()
{
     mConnectingInProgress = false;

     /* Connection ok, install Qt debug handler */
     qInstallMessageHandler(myMessageOutput);

     // Flush buffer contents first, if there is anything
     foreach(const QString& str, mBuffer) {
         mSocket.write(str.toUtf8());
         mSocket.write("\r\n");
     }
     mBuffer.clear();

}

void LogStreamThread::socketErrorSlot(QAbstractSocket::SocketError /* err */)
{
    mConnectingInProgress = false;
}

void LogStreamThread::udpSocketReadyReadSlot()
{
    /* We are expecting response packet from logger */
    while (mUDPDiscoverySocket.hasPendingDatagrams()) {
        QByteArray datagram;
        datagram.resize(mUDPDiscoverySocket.pendingDatagramSize());
        QHostAddress senderAddr;
        quint16 senderPort;
        mUDPDiscoverySocket.readDatagram(datagram.data(), datagram.size(),&senderAddr, &senderPort);
        QString myText = QString(datagram);

        /* Process datagram and create connection */
        /* Example messsage "ConnectTo;%1;%2"     */
        const int respPacketSlitCount = 3;
        QStringList mySplits  = myText.split(";");
        const QString discoveryResp("ConnectTo");

        if (mySplits.first()==discoveryResp && mySplits.size()==respPacketSlitCount ) {
            QString destIp = mySplits.at(1);
            int destIpPort = mySplits.at(2).toInt();

            if (mSocket.state() == QAbstractSocket::UnconnectedState) {
                connectToServer(destIp,destIpPort);
            }
        }
    }
}

void LogStreamThread::tryConnectionToServer()
{
    /* We dont't have access to remote. Try connecting to server after small timeOut */
    if (mConnectingInProgress==false ) {
        connectToServer();
    }
    else {
        if ( mConnSetupTimer.isActive()==false) {
            const int connectRetryTimerMs=1000;
            mConnSetupTimer.start(connectRetryTimerMs);
        }
    }
}

void LogStreamThread::connectToServer()
{

    if (QString(TIMETOPIC_LOGGING_SERVER_ADDRESS).length()>0) {

        mConnectingInProgress = true;

        mSocket.connectToHost(TIMETOPIC_LOGGING_SERVER_ADDRESS,
                              TIMETOPIC_LOGGING_SERVER_PORT,
                              QIODevice::WriteOnly);
    }
    else {
        /* Try discovery. Needs to match TimeToPic */
        const QString discoveryReq("Hello");
        const int udpDiscoveryPort = 5010;

        QByteArray datagram = discoveryReq.toUtf8();
        mUDPDiscoverySocket.writeDatagram(datagram.data(), datagram.size(),
                                 QHostAddress::Broadcast, udpDiscoveryPort);
    }
}

void LogStreamThread::connectToServer(QString destIp, int destPort)
{
    mConnectingInProgress = true;
    mSocket.connectToHost(destIp,
                          destPort,
                          QIODevice::WriteOnly);
}


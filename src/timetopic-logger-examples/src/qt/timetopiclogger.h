/* The MIT License (MIT)

Copyright (c) 2015 Herwood Technologies oy

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#ifndef TIMETOPICLOGGER_H
#define TIMETOPICLOGGER_H
#include <QThread>
#include <QTcpSocket>
#include <QUdpSocket>
#include <QElapsedTimer>
#include <QMutex>
#include <QTimer>

#include <QDebug>
#include <QTcpSocket>
#include <QMutex>
#include <QMutexLocker>
#include <QCoreApplication>

#ifdef Q_OS_WIN32
    #include <windows.h>
    #include <stdio.h>
    #include <psapi.h>
#endif

/* If discovery enabled, this app will try find logger from current IP subnet.
   Current implementation will connect to first responder.
   In normal workstation development (logger and debuggable app runs on same machine,
   discovery is not needed. */
//#define USEDISCOVERY

/* If USEDATETIME is enabled, normal timestamp is added instead elapsed time since start.
 * This is useful when multiple tranmitters are sending to same logger from same PC (common clock) */
//#define USEDATETIME


#ifdef USEDISCOVERY
    #define TIMETOPIC_LOGGING_SERVER_ADDRESS ""
    #define TIMETOPIC_LOGGING_SERVER_PORT 0
#else
    /* If you want specify destination address outside this computer / this subnet, override
     * TIMETOPIC_LOGGING_SERVER_ADDRESS default with your own. */
    #define TIMETOPIC_LOGGING_SERVER_ADDRESS "127.0.0.1"
    #define TIMETOPIC_LOGGING_SERVER_PORT 5000
#endif

#define BUFFER_MAX_LINE_COUNT 1000

// Define TTPLOGGING to enable these macros
#ifdef TTPLOGGING
#define TTPLOG_EVENTSCOPE(channel)      EventScopeObject obj(channel)
										// [%1]%2 is TimeToPic Logger Graphviz friendly (scope)
#define TTPLOG_FUNCTIONSCOPE            EventScopeObject funcObj(QString("[%1]%2").arg((int)QThread::currentThreadId()).arg(__FUNCTION__))
#define TTPLOG_EVENT(channel, state)    TimeToPicLogger::instance()->logEvent(channel, state)
#define TTPLOG_VALUE(channel, value)    TimeToPicLogger::instance()->logValue(channel, QString::number(value))
#define TTPLOG_STATE(channel, state)    TimeToPicLogger::instance()->logState(channel, state)
#define TTPLOG_STRING(str)              TimeToPicLogger::instance()->logString(str)
#define TTPLOG_LOG_OS_COUNTERS          TimeToPicLogger::instance()->enableOsPerfCounters(true);
#else
#define TTPLOG_EVENTSCOPE(channel)
#define TTPLOG_FUNCTIONSCOPE
#define TTPLOG_EVENT(channel, state)
#define TTPLOG_VALUE(channel, value)
#define TTPLOG_STATE(channel, state)
#define TTPLOG_STRING(str)
#define TTPLOG_LOG_OS_COUNTERS
#endif

/**
 * @brief This is an example implementation of a logger for Qt applications.
 *
 * Note! This implementation sends log rows to a remote logger: TimeToPicLogger in localhost.
 * Download it from: http://timetopic.herwoodtechnologies.com/
 *
 * This logger can be used from C++ and QML codes.
 *
 * An example of logging a function call:
 * @code{.cpp}
 * void example() {
 *     TTPLOG_EVENTSCOPE("Example function call");
 *     for(int i = 0; i < 5; i++) {
 *         sleep(1);
 *         TTPLOG_VALUE("Example value", i);
 *     }
 * }
 * @endcode
 * You should get log rows like this:
 * @code{.txt}
 * 0.0;event;start;Example function call
 * 1.0;value;0;Example value
 * 2.0;value;1;Example value
 * 3.0;value;2;Example value
 * 4.0;value;3;Example value
 * 5.0;value;4;Example value
 * 5.0001;event;start;Example function call
 * @endcode
 *
 * This logger can also be used from QML when code is initialized like this:
 * @code{.cpp}
 * QQmlApplicationEngine engine;
 * engine.rootContext()->setContextProperty("ttplogger", TimeToPicLogger::instance());
 * engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
 * @endcode
 *
 * Then in QML, you can call functions with Q_INVOKABLE like this:
 * @code{.qml}
 * Item {
 *     Component.onCompleted: {
 *         ttplogger.logEvent("Window", true)
 *     }
 *     Component.onDestruction: {
 *         ttplogger.logEvent("Window", false)
 *     }
 * }
 * @endcode
 */
class TimeToPicLogger : public QThread
{
    Q_OBJECT
public:

    /**
     * @brief This function returns pointer to singleton instance of this class.
     *
     * This also starts the log send streaming thread.
     * @return Pointer to this singleton instance.
     */
    static TimeToPicLogger *instance();

    void run();

    void stop();

    /**
     * @brief This enumerator is used when logging Event-type logs with logEvent.
     */
    enum EventState {
        EventStart,
        EventStop
    };

    /**
     * @brief This function logs any free text string.
     * @param text Any string. Does not have to be in TimeToPic format
     */
    Q_INVOKABLE void logString(const QString&  text);

    /**
     * @brief This function logs an event start or stop.
     *
     * This creates a log rows like this
     * @code
     * 10.1;event;start;Test channel
     * 10.8;event;stop;Test channel
     * @endcode
     * @param channel Name of the channel
     * @param eventState State of the event.
     */
    Q_INVOKABLE void logEvent(const QString& channel, EventState eventState);

    /**
     * @brief This funciton logs an event start or stop.
     *
     * See logEvent(const QString& channel, EventState eventState) for further details.
     */
    Q_INVOKABLE void logEvent(const QString& channel, bool startEvent);
    Q_INVOKABLE void logState(const QString& channel, const QString& state);
    Q_INVOKABLE void logValue(const QString& channel, const QString& value);

    /**
     * @brief This function start os performance counters
     *
     *
     */
    Q_INVOKABLE void enableOsPerfCounters(bool bOn);

signals:
    void newLogRow(QString text);

private slots:
    void performanceCounterTimerSlot();
private:
    void reportCPULoad();
    void reportProcessMemUse();
#ifdef Q_OS_WIN32
    ULONGLONG delta(const FILETIME& time1, const FILETIME& time2);
#endif

#ifdef Q_OS_LINUX
    QString getProcA(QString procItemKey, QString procItem);
    QString getProcB(int nthLineFromProc, QString procItem);
    qreal getCPUTimeProc();
    QString getThisProcessPID();
#endif

    TimeToPicLogger();
    QString getTimestamp();

    static TimeToPicLogger *m_instance;
    QMutex m_mutex;
    QElapsedTimer m_elapsed;
    QTimer m_performanceCounterTimer;
    qint64 m_psMemoryUSed;
    qint16 m_cpuLoadPercent;
    qreal  m_prevProcessTime;
    qreal  m_prevTotalTime;
#ifdef Q_OS_WIN32
    FILETIME m_ftPrevSysKernel;
    FILETIME m_ftPrevSysUser;

    //process times
    FILETIME m_ftPrevProcKernel;
    FILETIME m_ftPrevProcUser;
#endif

#ifdef Q_OS_LINUX
#endif

};



class EventScopeObject
{
public:
    EventScopeObject(const QString& channel) : m_channel(channel)
    {
#ifdef TTPLOGGING
        TimeToPicLogger::instance()->logEvent(channel, TimeToPicLogger::EventStart);
#endif
    }
    ~EventScopeObject()
    {
#ifdef TTPLOGGING
        TimeToPicLogger::instance()->logEvent(m_channel, TimeToPicLogger::EventStop);
#endif
    }
private:
    QString m_channel;
};


class LogStreamThread : public QObject
{
    Q_OBJECT
public:

    LogStreamThread();

public slots:

    void writeRow(QString txt);
private slots:
    void connectionTryTimerTimeTimeoutSlot();
    void socketDisconnectedSlot();
    void socketConnectedSlot();
    void socketErrorSlot(QAbstractSocket::SocketError err);
    void udpSocketReadyReadSlot();
private:

    void tryConnectionToServer();
    void connectToServer();
    void connectToServer(QString destIp,int destPort);

    bool mConnectingInProgress;
    QStringList mBuffer;
    QTcpSocket mSocket;
    QTimer mConnSetupTimer;

    QUdpSocket mUDPDiscoverySocket;

};


#endif // TIMETOPICLOGGER_H
